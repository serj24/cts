package com.example.proiect_android_tari;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitateAdaugaTara extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitate_adauga_tara);
	}
	public void buttonAdaugaTaraOnClick(View view){
		
		DAOTari dao = new DAOTari(this);
        dao.open();
        EditText textTara=(EditText)findViewById(R.id.editTextNumeTara);
		EditText textDescriere=(EditText)findViewById(R.id.editTextDescriereTara);
		String numeTara=textTara.getText().toString();
		String descriereTara=textDescriere.getText().toString();
		if(numeTara.equals("") || descriereTara.equals("")){
			Toast.makeText(this, "Exista campuri necompletate!", Toast.LENGTH_LONG).show();
		}
		else{
		Tara t =new Tara(numeTara);
		t.setDescriere(descriereTara);
		try {
			dao.adaugaTara(t);
		} catch (AlreadyExistingDataException e) {
			Toast.makeText(this, "Acesta tara exista deja in baza de date!", Toast.LENGTH_LONG).show();
		}
		ListaTari.ListaTari=dao.obtineToateTarile();
		dao.close();
		Toast.makeText(this,"Tara adaugata!", Toast.LENGTH_SHORT).show();
		textTara.setText("");
		textDescriere.setText("");
		}
	}
}
