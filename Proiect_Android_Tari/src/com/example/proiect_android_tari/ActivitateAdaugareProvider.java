package com.example.proiect_android_tari;

import java.util.Calendar;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.IpValidator;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitateAdaugareProvider extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitate_adauga_provider);
	}
	
	public void buttonAdaugaProviderOnClick(View view) throws AlreadyExistingDataException
	{
		String date = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
       
		EditText textTara=(EditText)findViewById(R.id.editTextTara);
		EditText textProvider=(EditText)findViewById(R.id.editTextProvider);
		String numeTara=textTara.getText().toString();
		String numeProvider=textProvider.getText().toString();
		EditText textFrom=(EditText)findViewById(R.id.editTextFrom);
		EditText textTo=(EditText)findViewById(R.id.editTextTo);
		String numeFrom=textFrom.getText().toString();
		String numeTo=textTo.getText().toString();
		if(numeTara.equals("") || numeProvider.equals("") ||numeFrom.equals("") || numeTo.equals("")){
			Toast.makeText(this, "Exista campuri necompletate!", Toast.LENGTH_LONG).show();
		}
		else{
			
		boolean goodIp1=IpValidator.validateV4AndV6(numeFrom);
		boolean goodIp2=IpValidator.validateV4AndV6(numeTo);
		
		if(!goodIp1 || !goodIp2){
			Toast.makeText(this,"Ip incorect!",Toast.LENGTH_LONG).show();
		}else{
		DAOTari dao = new DAOTari(this);
        dao.open();
        try {
			dao.adaugaRange(numeProvider, numeTara, numeFrom, numeTo);
		} catch (InvalidIpDataFormat e) {
			Toast.makeText(this,"Ip incorect!",Toast.LENGTH_LONG).show();
		} catch (InvalidIpSetException e) {
			Toast.makeText(this,"Ip set incorect!",Toast.LENGTH_LONG).show();
		}
        ListaTari.ListaTari=dao.obtineToateTarile();
        dao.close();
        textTara.setText("");
        textProvider.setText("");
        textFrom.setText("");
        textTo.setText("");
		}
		}
		if(!ListaTari.ExistaTara(numeTara))
		{
			Toast.makeText(this,"Nu exista nici o tara cu acest nume!Va fi adaugata o noua tara.", Toast.LENGTH_SHORT).show();
		    Tara tara=new Tara(numeTara);
		    Provider p=new Provider(numeProvider, numeTara);
			SetIP sIP=new SetIP(numeFrom,numeTo,date);
			p.AddIpRange(sIP);
			tara.AddProvider(p);
			ListaTari.Add(tara);	
		}
		else
		{
			Tara tara=ListaTari.GetTara(numeTara);
			if(tara.ExistaProvider(numeProvider))
			{
				Provider p=tara.GetProvider(numeProvider);
				SetIP sIP=new SetIP(numeFrom,numeTo,date);
				if(p.ExistaSetIP(sIP))
				{
					Toast.makeText(this,"Exista deja acest Range!", Toast.LENGTH_SHORT).show();
				}
				else
				{
				   p.AddIpRange(sIP);
				}
			}
			else
			{
				Provider p=new Provider(numeProvider, numeTara);
				SetIP sIP=new SetIP(numeFrom,numeTo,date);
				p.AddIpRange(sIP);
			}
		}
	}
	
	@Override
    public void onDestroy()
    {
		super.onDestroy();
    	//ListaTari.ListaTari.clear();
    }
}
