package com.example.proiect_android_tari;

import java.util.ArrayList;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitateBD extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activitate_bd);
	}

	public void onClickUpdateAll(View view) {
		final Tara t = new Tara("Romania");
		final DAOTari dao2 = new DAOTari(this);
		try {
			PreluareIpuri pi = new PreluareIpuri() {
				@Override
				protected void onPostExecute(ArrayList<Provider> rezultat) {

					dao2.open();
					for (Provider p : rezultat) {
						Log.v("test", t.getNume() + p.getName() + " "
								+ p.getListaIp().get(0).getToIP() + ":"
								+ p.getListaIp().get(0).getFromIP());
						t.AddProvider(p);
						try {
							dao2.adaugaRange(p.getName(), t.getNume(), p
									.getListaIp().get(0).getFromIP(), p
									.getListaIp().get(0).getToIP());
						} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
						} catch (InvalidIpSetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ListaTari.ListaTari = dao2.obtineToateTarile();

					}
					dao2.close();
				}
			};
			pi.execute("http://www.nirsoft.net/countryip/ro.html", t.getNume());

			final Tara t2 = new Tara("Franta");
			pi = new PreluareIpuri() {
				@Override
				protected void onPostExecute(ArrayList<Provider> rezultat) {

					dao2.open();
					for (Provider p : rezultat) {
						Log.v("test", t2.getNume() + p.getName() + " "
								+ p.getListaIp().get(0).getToIP() + ":"
								+ p.getListaIp().get(0).getFromIP());
						t2.AddProvider(p);
						try {
							dao2.adaugaRange(p.getName(), t2.getNume(), p
									.getListaIp().get(0).getFromIP(), p
									.getListaIp().get(0).getToIP());
						} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
						} catch (InvalidIpSetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ListaTari.ListaTari = dao2.obtineToateTarile();

					}
					dao2.close();
				}
			};
			pi.execute("http://www.nirsoft.net/countryip/fr.html", t2.getNume());

			final Tara t3 = new Tara("Statele Unite ale Americii");
			pi = new PreluareIpuri() {
				@Override
				protected void onPostExecute(ArrayList<Provider> rezultat) {

					dao2.open();
					for (Provider p : rezultat) {
						Log.v("test", t3.getNume() + p.getName() + " "
								+ p.getListaIp().get(0).getToIP() + ":"
								+ p.getListaIp().get(0).getFromIP());
						t3.AddProvider(p);
						try {
							dao2.adaugaRange(p.getName(), t3.getNume(), p
									.getListaIp().get(0).getFromIP(), p
									.getListaIp().get(0).getToIP());
						} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
						} catch (InvalidIpSetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ListaTari.ListaTari = dao2.obtineToateTarile();

					}
					dao2.close();
				}
			};
			pi.execute("http://www.nirsoft.net/countryip/us.html", t3.getNume());

			final Tara t4 = new Tara("Germania");
			pi = new PreluareIpuri() {
				@Override
				protected void onPostExecute(ArrayList<Provider> rezultat) {

					dao2.open();
					for (Provider p : rezultat) {
						Log.v("test", t4.getNume() + p.getName() + " "
								+ p.getListaIp().get(0).getToIP() + ":"
								+ p.getListaIp().get(0).getFromIP());
						t4.AddProvider(p);
						try {
							dao2.adaugaRange(p.getName(), t4.getNume(), p
									.getListaIp().get(0).getFromIP(), p
									.getListaIp().get(0).getToIP());
						} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
						} catch (InvalidIpSetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						ListaTari.ListaTari = dao2.obtineToateTarile();

					}
					dao2.close();
				}
			};
			pi.execute("http://www.nirsoft.net/countryip/de.html", t4.getNume());
		} catch (Exception e) {
			Toast.makeText(this, "Conectati-va la retea", Toast.LENGTH_LONG)
					.show();
		}

	}

	public void onClickAddCountrytoDatabase(View view) {
		EditText textTara = (EditText) findViewById(R.id.editTextCountry);
		EditText textCode = (EditText) findViewById(R.id.editTextCode);
		EditText textDescriere = (EditText) findViewById(R.id.editTextDescriere);
		if (textTara.getText().toString().equals("")
				|| textCode.getText().toString().equals("")) {
			Toast.makeText(this, "Exista cammpuri necompletate!",
					Toast.LENGTH_LONG).show();
		} else {
			final Tara t = new Tara(textTara.getText().toString());
			t.setDescriere(textDescriere.getText().toString());
			final DAOTari dao2 = new DAOTari(this);
			try {
				PreluareIpuri pi = new PreluareIpuri() {
					@Override
					protected void onPostExecute(ArrayList<Provider> rezultat) {

						dao2.open();
						try {
							dao2.adaugaTara(t);
						} catch (AlreadyExistingDataException e1) {
						}
						for (Provider p : rezultat) {
							Log.v("test", t.getNume() + p.getName() + " "
									+ p.getListaIp().get(0).getToIP() + ":"
									+ p.getListaIp().get(0).getFromIP());
							t.AddProvider(p);
							try {
								dao2.adaugaRange(p.getName(), t.getNume(), p
										.getListaIp().get(0).getFromIP(), p
										.getListaIp().get(0).getToIP());
							} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
							} catch (InvalidIpSetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							ListaTari.ListaTari = dao2.obtineToateTarile();

						}
						dao2.close();
					}
				};
				pi.execute(
						"http://www.nirsoft.net/countryip/"
								+ textCode.getText().toString().toLowerCase()
								+ ".html", t.getNume());
				Toast.makeText(this, "Tara adaugata!", Toast.LENGTH_LONG)
						.show();
				textTara.setText("");
				textCode.setText("");
				textDescriere.setText("");
			} catch (Exception e) {
				Toast.makeText(this, "Conectati-va la retea", Toast.LENGTH_LONG)
						.show();
			}
		}
	}
}
