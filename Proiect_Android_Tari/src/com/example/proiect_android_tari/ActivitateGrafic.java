package com.example.proiect_android_tari;

import java.util.ArrayList;
import java.util.List;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.Grafic.Histogram;
import com.example.proiect_android_tari.exceptions.CountryNotFoundException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ActivitateGrafic extends Activity {

	public static List<Histogram> Histograma;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_activitate_grafic);
		//InitalizareGrafic();
		/*DAOTari dao=new DAOTari(this);
		dao.open();
		String tara=getIntent().getStringExtra("Tara");
		Tara t=dao.selectTara(tara);
		List<Provider>l=dao.obtineTotiProvideriTara(t.getId());
		List<SetIP>s=dao.obtineSeturiProvider(1);
		dao.close();*/
		try {
			Histograma=getHistogram(new DAOTari(this));
		} catch (CountryNotFoundException e) {
			Histograma=new ArrayList<Histogram>();
		}
	}
	
	public List<Histogram> getHistogram(DAOTari dao) throws CountryNotFoundException{
		List<Histogram> lista=new ArrayList<Histogram>();
		String tara=getIntent().getStringExtra("Tara");
		dao.open();
		Tara t=dao.selectTara(tara);
		if(t==null){
			throw new CountryNotFoundException();
		}
		List<Provider> list=dao.obtineTotiProvideriTara(t.getId());
		for(int i=0;i<list.size();i++){
			Provider p=list.get(i);
			if(p!=null){
				Histogram h =new Histogram(p.getName(),0);
				int id=dao.selectProvider(p.getName()).getId();
				List<SetIP> l=dao.obtineSeturiProvider(id);
				int numar=0;
				for(int j=0;j<l.size();j++)
				{
					if(l.get(j)!=null)numar++;
				}
				h.setNumar(numar);
				lista.add(h);
				Log.i("Provider",p.getName());
				Log.i("Numar",String.valueOf(l.size()));
			}
		}
		dao.close();
		return lista;
	}
}
