package com.example.proiect_android_tari;




import com.example.proiect_android_tari.Database.DAOTari;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ActivitateIstoric extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitate_istoric);
		DAOTari dao=new DAOTari(this);
		dao.open();
		ActivitatePrincipala.ipuri=dao.obtineLoguri();
		dao.close();
		ListView lista = (ListView) findViewById(R.id.listViewIstoricIpuri);
        ArrayAdapter<HistoricLogIP> adaptor = new ArrayAdapter<HistoricLogIP>(this, android.R.layout.simple_list_item_1, ActivitatePrincipala.ipuri);
        lista.setAdapter(adaptor);
	}
	
	public void Back(View v){
		this.finish();
	}
}
