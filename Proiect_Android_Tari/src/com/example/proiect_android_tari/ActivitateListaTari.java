package com.example.proiect_android_tari;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class ActivitateListaTari extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitate_lista_tari);
		ListView listView=(ListView)findViewById(R.id.listViewTari);
        ArrayAdapter<Tara> adapter=new ArrayAdapter<Tara>(this, android.R.layout.simple_list_item_1,ListaTari.ListaTari);
        listView.setAdapter(adapter);
	
	}
	  @Override
	   protected void onResume()
	   {
		super.onResume();
	   	ListView listView=(ListView)findViewById(R.id.listViewTari);
	       ArrayAdapter<Tara> adapter=new ArrayAdapter<Tara>(this, android.R.layout.simple_list_item_1,ListaTari.ListaTari);
	       listView.setAdapter(adapter);
	 }
	
	  public void inapoi(View v)
	  {
		  this.finish();
	  }
	
}
