package com.example.proiect_android_tari;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.Retea.PreluareTari;
import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.IpValidator;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitatePrincipala extends Activity {
	public static ArrayList<HistoricLogIP> ipuri = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// PopuleazaLista();
		// PreluareListaTari();
		DAOTari dao = new DAOTari(this);
		dao.open();
		ipuri = dao.obtineLoguri();
		ListaTari.ListaTari = (ArrayList<Tara>) dao.obtineToateTarile();
		dao.close();
		setContentView(R.layout.actvitate_principala);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.action_despre) {
			Intent intent = new Intent(this, ActivitateDespre.class);
			startActivity(intent);
		}
		if (id == R.id.action_db) {
			Intent intent = new Intent(this, ActivitateBD.class);
			startActivity(intent);
		}

		if (id == R.id.action_istoric) {
			Intent intent = new Intent(this, ActivitateIstoric.class);
			startActivity(intent);
		}

		if (id == R.id.action_adauga_provider) {
			Intent intent = new Intent(this, ActivitateAdaugareProvider.class);
			startActivity(intent);
			return true;
		}

		if (id == R.id.action_tari_expandable) {
			Intent intent = new Intent(this, Activitate_Tari_Expandable.class);
			startActivity(intent);
			return true;
		}
		if (id == R.id.action_adauga_tara) {
			Intent intent = new Intent(this, ActivitateAdaugaTara.class);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void adaugaIP(String ipul) {

		Date data;

		Calendar c = Calendar.getInstance();
		int sec = c.get(Calendar.SECOND);
		int min = c.get(Calendar.MINUTE);
		int hour = c.get(Calendar.HOUR);

		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		data = new Date(year, month, day, hour, min, sec);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy   HH:mm:ss");
		HistoricLogIP log = new HistoricLogIP(ipul, data.toLocaleString());

		DAOTari dao = new DAOTari(this);
		dao.open();
		dao.adaugaLog(log);
		ActivitatePrincipala.ipuri = dao.obtineLoguri();
		dao.close();
		// this.ipuri.add(log);
		/*
		 * for (String s : this.ipuri) { Toast.makeText(this, s,
		 * Toast.LENGTH_SHORT).show(); }
		 */
		EditText text = (EditText) findViewById(R.id.editTextIP);
		text.setText("");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		ListaTari.ListaTari.clear();
	}

	public void onClick(View v) {
		String Ip = ((EditText) findViewById(R.id.editTextIP)).getText()
				.toString();

		if (Ip.length() == 0) {
			Toast.makeText(this, "Nu exista nici un ip introdus!",
					Toast.LENGTH_LONG).show();
		} else {
			boolean isGood = IpValidator.validateV4AndV6(Ip);

			if (!isGood) {
				Toast.makeText(this, "Forma Ip gresita!", Toast.LENGTH_LONG)
						.show();
			} else {
				DAOTari dao = new DAOTari(this);
				dao.open();
				SetIP ip;
				try {
					ip = dao.cautaIp(Ip);
				adaugaIP(Ip);
				dao.close();
				if (ip == null) {
					Toast.makeText(this, "Nu exista Ip-ul specificat",
							Toast.LENGTH_LONG).show();
				} else {

					Toast.makeText(
							this,
							ip.getProvider().getName() + " "
									+ ip.getProvider().getTara(),
							Toast.LENGTH_SHORT).show();
					try {
						Intent intent = new Intent(this,
								Activitate_Rezultat.class);
						intent.putExtra("Provider", ip.getProvider().getName());
						intent.putExtra("Tara", ip.getProvider().getTara());
						intent.putExtra("Ip", Ip);
						intent.putExtra("IpFrom", ip.getFromIP());
						intent.putExtra("IpTo", ip.getToIP());
						intent.putExtra("Ip", Ip);
						startActivity(intent);
						// finish();
					} finally {
					}
					}
					} catch (InvalidIpDataFormat e) {
						Toast.makeText(this, "Forma Ip gresita!", Toast.LENGTH_LONG)
						.show();
					}
				}
			}
		}
		/*
		 * Provider provider=null; int ipSetNumber=0; for(int
		 * i=0;i<ListaTari.size();i++) { for(int
		 * j=0;j<ListaTari.get(i).getProvideri().size();j++) { Provider
		 * p=ListaTari.get(i).getProvideri().get(j); if(p.SearchForIP(Ip)!=-1){
		 * provider=p; ipSetNumber=(int)p.SearchForIP(Ip); break; } } }
		 * if(provider!=null) {
		 * 
		 * Toast.makeText(this,provider.Name+" "+provider.Tara,
		 * Toast.LENGTH_SHORT).show(); try{ Intent intent=new
		 * Intent(this,Activitate_Rezultat.class);
		 * intent.putExtra("Provider",provider.getName());
		 * intent.putExtra("Tara", provider.getTara()); intent.putExtra("Ip",
		 * Ip); intent.putExtra("IpSetNumber",ipSetNumber);
		 * startActivity(intent); // finish(); } finally{
		 * 
		 * } } else
		 * Toast.makeText(this,"Nu exista Ip-ul specificat",Toast.LENGTH_LONG
		 * ).show(); adaugaIP(); }
		 */

	public void PreluareListaTari() {
		PreluareTari ps = new PreluareTari() {
			String rez;

			@Override
			protected void onPostExecute(String rezultat) {

				rez = rezultat;
				if (rez.length() > 0)
					Log.i("tst", rez);
			}
		};
		ps.execute("http://oorsprong.org/websamples.countryinfo/CountryInfoService.wso/FullCountryInfoAllCountries");
	}

	public void onClickDatabase(View v) {
		final Tara t = new Tara("Romania");
		final DAOTari dao2 = new DAOTari(this);

		PreluareIpuri pi = new PreluareIpuri() {
			@Override
			protected void onPostExecute(ArrayList<Provider> rezultat) {

				dao2.open();
				for (Provider p : rezultat) {
					Log.v("test", t.getNume() + p.getName() + " "
							+ p.getListaIp().get(0).getToIP() + ":"
							+ p.getListaIp().get(0).getFromIP());
					t.AddProvider(p);
					try {
						dao2.adaugaRange(p.getName(), t.getNume(), p.getListaIp()
								.get(0).getFromIP(), p.getListaIp().get(0)
								.getToIP());
					} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
					} catch (InvalidIpSetException e) {
						

					}
					ListaTari.ListaTari = dao2.obtineToateTarile();
				}
				dao2.close();
			}
		};
		pi.execute("http://www.google.com", t.getNume());

		final Tara t2 = new Tara("Franta");
		pi = new PreluareIpuri() {
			@Override
			protected void onPostExecute(ArrayList<Provider> rezultat) {

				dao2.open();
				for (Provider p : rezultat) {
					Log.v("test", t2.getNume() + p.getName() + " "
							+ p.getListaIp().get(0).getToIP() + ":"
							+ p.getListaIp().get(0).getFromIP());
					t2.AddProvider(p);
					try {
						dao2.adaugaRange(p.getName(), t2.getNume(), p.getListaIp()
								.get(0).getFromIP(), p.getListaIp().get(0)
								.getToIP());
					} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
					} catch (InvalidIpSetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					ListaTari.ListaTari = dao2.obtineToateTarile();

				}
				dao2.close();
			}
		};
		pi.execute("http://www.nirsoft.net/countryip/fr.html", t2.getNume());

		final Tara t3 = new Tara("Sua");
		pi = new PreluareIpuri() {
			@Override
			protected void onPostExecute(ArrayList<Provider> rezultat) {

				dao2.open();
				for (Provider p : rezultat) {
					Log.v("test", t3.getNume() + p.getName() + " "
							+ p.getListaIp().get(0).getToIP() + ":"
							+ p.getListaIp().get(0).getFromIP());
					t3.AddProvider(p);
					try {
						dao2.adaugaRange(p.getName(), t3.getNume(), p.getListaIp()
								.get(0).getFromIP(), p.getListaIp().get(0)
								.getToIP());
					} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
						
					} catch (InvalidIpSetException e) {
					}
					ListaTari.ListaTari = dao2.obtineToateTarile();

				}
				dao2.close();
			}
		};
		pi.execute("http://www.nirsoft.net/countryip/us.html", t3.getNume());

		final Tara t4 = new Tara("Germania");
		pi = new PreluareIpuri() {
			@Override
			protected void onPostExecute(ArrayList<Provider> rezultat) {

				dao2.open();
				for (Provider p : rezultat) {
					Log.v("test", t4.getNume() + p.getName() + " "
							+ p.getListaIp().get(0).getToIP() + ":"
							+ p.getListaIp().get(0).getFromIP());
					t4.AddProvider(p);
					try {
						dao2.adaugaRange(p.getName(), t4.getNume(), p.getListaIp()
								.get(0).getFromIP(), p.getListaIp().get(0)
								.getToIP());
					} catch (InvalidIpDataFormat | AlreadyExistingDataException e) {
					} catch (InvalidIpSetException e) {
					}
					ListaTari.ListaTari = dao2.obtineToateTarile();

				}
				dao2.close();
			}
		};
		pi.execute("http://www.nirsoft.net/countryip/de.html", t4.getNume());
	}
}
