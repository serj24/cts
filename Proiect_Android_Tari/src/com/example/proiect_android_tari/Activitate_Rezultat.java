package com.example.proiect_android_tari;

import com.example.proiect_android_tari.Database.DAOTari;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Activitate_Rezultat extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activitate__rezultat);
		try{
		String Provider=getIntent().getStringExtra("Provider");
		String Tara=getIntent().getStringExtra("Tara");
		String IpSetFrom=getIntent().getStringExtra("IpFrom");
		String IpSetTo=getIntent().getStringExtra("IpTo");
		String Ip=getIntent().getStringExtra("Ip");
		TextView textTara=(TextView)findViewById(R.id.textViewRezultatTara);
		TextView textProvider=(TextView)findViewById(R.id.textViewRezultatProvider);
		TextView textDescriere=(TextView)findViewById(R.id.editTextDescriereRezultat);
		TextView textIp=(TextView)findViewById(R.id.textViewIpRezultat);
		TextView textIpLeft=(TextView)findViewById(R.id.textViewIpRezultatLeft);
		TextView textIpRight=(TextView)findViewById(R.id.textViewIpRezultatRight);
		ImageView imageSteag=(ImageView)findViewById(R.id.imageViewRezultatFlag);
		//Tara tara=ListaTari.GetTara(Tara);
		//Provider provider=tara.GetProvider(Provider);
		textTara.setText(Tara);
		textProvider.setText(Provider);
		DAOTari dao=new DAOTari(this);
		dao.open();
		Tara t=dao.selectTara(Tara);
		dao.close();
		textDescriere.setText(t.getDescriere());
		textIp.setText(Ip);
		textIp.setTextColor(Color.RED);
		textIpRight.setText(IpSetFrom);
		textIpRight.setTextColor(Color.BLUE);
		textIpLeft.setText(IpSetTo);
		textIpLeft.setTextColor(Color.BLUE);
		int id = getResources().getIdentifier(Tara.toLowerCase(), "drawable", getPackageName());
		imageSteag.setImageResource(id);
		
		}
		finally{
	
		}
		}
}
