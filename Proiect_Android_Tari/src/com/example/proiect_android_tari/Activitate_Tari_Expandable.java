package com.example.proiect_android_tari;


import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class Activitate_Tari_Expandable extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity__tari__expandable);
		
		AdapterTari adapter=new AdapterTari(this,R.layout.custom_element,ListaTari.ListaTari);
		setListAdapter(adapter);
		
	}
	@Override
    protected void onListItemClick(ListView l, View v, int position, long id){
        super.onListItemClick(l, v, position, id);
        Intent intent=new Intent(this,ActivitateGrafic.class);
        Tara t = (Tara)this.getListAdapter().getItem(position);
        Toast.makeText(this, "Tara: "+t.getNume(), Toast.LENGTH_LONG).show();
	    intent.putExtra("Tara",t.getNume());
	    startActivity(intent);
    }
}
