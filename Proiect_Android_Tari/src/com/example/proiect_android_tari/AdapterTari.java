package com.example.proiect_android_tari;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.Inflater;

import android.R.drawable;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class AdapterTari extends BaseAdapter {

	public class ViewHolder
	{
		public TextView textNumeTara,textDetalii;
		public ImageView flag;
		//public ExpandableListView listaProvideri;
	}
	public List<Tara> listaTari;
	LayoutInflater layoutInflater;
	Context context2;
	public AdapterTari(Context context,int res,List<Tara> lista)
	{
		layoutInflater=LayoutInflater.from(context);
		listaTari=lista;
		context2=context;
	}
	@Override
	public int getCount() {
		return listaTari.size();
				
	}

	@Override
	public Object getItem(int position) {
		return listaTari.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
  private List<Map<String, String>> createGroupList(Tara tara) {
          List<Map<String, String>> result = new ArrayList<Map<String, String>>();
          for( int i = 0 ; i < tara.getProvideri().size() ; i++ ) {
            Map map = new HashMap<String, String>();
            map.put("Provider",tara.getProvideri().get(i).getName()); 
            result.add( map );
          }
          return result;
    }
  private List<List<Map<String, String>>>  createChildList(Tara tara) {
	  
      List<List<Map<String, String>>>  result = new ArrayList<List<Map<String, String>>>();
      for( int i = 0 ; i < tara.getProvideri().size() ; i++) { 
        List<Map<String, String>> secList = new ArrayList<Map<String, String>>();
        for( int n = 0 ; n < tara.getProvideri().get(i).ListaIp.size() ; n++ ) {
          Map<String, String>  child = new HashMap<String, String> ();
          child.put( "SetIP",  tara.getProvideri().get(i).ListaIp.get(n).toString() );
          secList.add( child );
        }
       result.add( secList );
      }
      return result;
  }
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
    View view;
    ViewHolder holder=null;
    if(convertView==null)
    {
    	view=layoutInflater.inflate(R.layout.custom_element,parent,false);
    	holder=new ViewHolder();
    	holder.textNumeTara=(TextView)view.findViewById(R.id.textNumeTara);
    	holder.textDetalii=(TextView)view.findViewById(R.id.textDetalii);
    	holder.flag=(ImageView)view.findViewById(R.id.imageFlag);
    	//holder.listaProvideri=(ExpandableListView)view.findViewById(R.id.expandableListViewProvideri);
        view.setTag(holder);
        Log.i("2","2");
    }
    else
    {
    	view=convertView;
    	holder=(ViewHolder)view.getTag();
    }
        Tara tara=listaTari.get(position);
        holder.textNumeTara.setText(tara.getNume());
        holder.textDetalii.setText(tara.getDescriere());
        int id =  context2.getResources().getIdentifier(tara.getNume().toLowerCase(), "drawable", context2.getPackageName());
        if(id<0) id =  context2.getResources().getIdentifier("noimage", "drawable", context2.getPackageName());
        holder.flag.setImageResource(id);
      /*  SimpleExpandableListAdapter listAdapter =
                 new SimpleExpandableListAdapter(
                         context2,
                         createGroupList(tara),              
                         R.layout.list_provider_rows,            
                         new String[] { "Provider" },  
                         new int[] { R.id.Provider },    
                         createChildList(tara),            
                         R.layout.list_child_rows,        
                         new String[] {"SetIP"},      
                         new int[] { R.id.setIP}    
                     );*/
       // holder.listaProvideri.setDividerHeight(2);
       // holder.listaProvideri.setClickable(true);
       // holder.listaProvideri.setAdapter( listAdapter ); 
		return view;
	}
	
	

}
