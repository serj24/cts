package com.example.proiect_android_tari.Database;

import java.io.InvalidObjectException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.example.proiect_android_tari.HistoricLogIP;
import com.example.proiect_android_tari.Provider;
import com.example.proiect_android_tari.SetIP;
import com.example.proiect_android_tari.Tara;
import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.IpFormat;
import com.example.proiect_android_tari.Util.IpTransformer;
import com.example.proiect_android_tari.Util.IpValidator;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAOTari {

	DbManager dbManager = null;
	SQLiteDatabase bd = null;

	public void open() {
		if (dbManager == null) {
			throw new InvalidParameterException("dbManager is null");
		}
		bd = dbManager.getWritableDatabase();
	}

	public void close() {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		if (!bd.isOpen()) {
			throw new UnsupportedOperationException(
					"sqlDatabase already closed");
		}
		bd.close();
	}

	public DAOTari(Context context) {
		if (context == null) {
			throw new InvalidParameterException();
		}
		dbManager = new DbManager(context);
	}

	public DAOTari(DbManager dbManager) {
		if (dbManager == null) {
			throw new InvalidParameterException("dbManager can not be null");
		}
		this.dbManager = dbManager;
	}

	public DAOTari(Context context, String dbName) {
		if (context == null || dbName == null || dbName.equals("")) {
			throw new InvalidParameterException();
		}
		dbManager = new DbManager(context, dbName);
	}

	public long adaugaLog(HistoricLogIP t) {
		if (t == null) {
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		ContentValues inregistrare = new ContentValues();
		inregistrare.put(DbConstants.Ip, t.getIp());
		inregistrare.put(DbConstants.Data, t.getData());
		return bd.insert(DbConstants.NumeTabelaLog, null, inregistrare);
	}

	public long adaugaTara(Tara t) throws AlreadyExistingDataException {
		if (t == null) {
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		if (selectTara(t.getNume()) != null) {
			throw new AlreadyExistingDataException();
		}
		ContentValues inregistrare = new ContentValues();
		inregistrare.put(DbConstants.Nume, t.getNume());
		inregistrare.put(DbConstants.Descriere, t.getDescriere());
		return bd.insert(DbConstants.NumeTabelaTari, null, inregistrare);
	}

	public long adaugaProvider(String denumire, String numeTara)
			throws AlreadyExistingDataException {
		if (denumire == null || numeTara == null || denumire.equals("")
				|| numeTara.equals("")) {
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		if (selectProvider(denumire) != null) {
			throw new AlreadyExistingDataException();
		}
		Tara t = selectTara(numeTara);
		if (t == null) {
			adaugaTara(new Tara(numeTara));
			t = selectTara(numeTara);
		}
		ContentValues inregistrare = new ContentValues();
		inregistrare.put(DbConstants.Denumire, denumire);
		inregistrare.put(DbConstants.Id_tara, t.getId());
		return bd.insert(DbConstants.NumeTabelaProvideri, null, inregistrare);
	}

	public long adaugaRange(String numeProvider, String numeTara, String from,
			String to) throws InvalidIpDataFormat,
			AlreadyExistingDataException, InvalidIpSetException {
		if (numeProvider == null || numeTara == null || numeProvider.equals("")
				|| numeTara.equals("") || from == null || from.equals("")
				|| to.equals("") || to == null) {
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		Provider p = selectProvider(numeProvider);
		if (p == null) {
			adaugaProvider(numeProvider, numeTara);
			p = selectProvider(numeProvider);
		}
		String format;
		IpValidator validator = new IpValidator();
		if (validator.validate(from)) {
			format = IpFormat.Ipv4.toString();
		} else {
			format = IpFormat.Ipv6.toString();
		}
		ContentValues inregistrare = new ContentValues();
		long[] fromL = IpTransformer.tranformIpToNumericLong(from);
		long[] toL = IpTransformer.tranformIpToNumericLong(to);
		if (fromL.length == 1 && toL.length == 1) {
			if (fromL[0] >= toL[0]) {
				throw new InvalidIpSetException();
			}
			inregistrare.put(DbConstants.Inceput1, fromL[0]);
			inregistrare.put(DbConstants.Sfarsit1, toL[0]);
			inregistrare.put(DbConstants.Inceput2, 0);
			inregistrare.put(DbConstants.Sfarsit2, 0);
			inregistrare.put(DbConstants.Tip, IpFormat.Ipv4.toString());
			inregistrare.put(DbConstants.Id_Provider, p.getId());
		} else {
			if (fromL[0] > toL[0] || fromL[1] > toL[1]) {
				throw new InvalidIpSetException();
			}
			inregistrare.put(DbConstants.Inceput1, fromL[0]);
			inregistrare.put(DbConstants.Sfarsit1, toL[0]);
			inregistrare.put(DbConstants.Inceput2, fromL[1]);
			inregistrare.put(DbConstants.Sfarsit2, toL[1]);
			inregistrare.put(DbConstants.Tip, IpFormat.Ipv6.toString());
			inregistrare.put(DbConstants.Id_Provider, p.getId());
		}
		return bd.insert(DbConstants.NumeTabelaSeturiIp, null, inregistrare);
	}

	// select tara dupa ID
	public Tara selectTara(int Id) {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		String[] whereArgs = new String[] { String.valueOf(Id) };
		Cursor cursor = bd.query(DbConstants.NumeTabelaTari, null,
				DbConstants.Id + "=?", whereArgs, null, null, null);
		Tara t = null;
		if (cursor.moveToNext()) {
			t = new Tara();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String nume = cursor.getString(DbConstants.IndexCol2);
			String descriere = cursor.getString(DbConstants.IndexCol3);
			t.setNume(nume);
			t.setDescriere(descriere);
			t.setId(id);
		}

		return t;
	}

	public Tara selectTara(String numeTara) {
		if(numeTara==null || numeTara.equals("")){
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		String[] whereArgs = new String[] { numeTara };
		Cursor cursor = bd.query(DbConstants.NumeTabelaTari, null,
				DbConstants.Nume + "=?", whereArgs, null, null, null);
		Tara t = null;
		if (cursor.moveToNext()) {
			t = new Tara();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String nume = cursor.getString(DbConstants.IndexCol2);
			String descriere = cursor.getString(DbConstants.IndexCol3);
			t.setNume(nume);
			t.setDescriere(descriere);
			t.setId(id);
		}

		return t;
	}

	// select provider dupa Id
	public Provider selectProvider(int Id) {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		String[] whereArgs = new String[] { String.valueOf(Id), };
		Cursor cursor = bd.query(DbConstants.NumeTabelaProvideri, null,
				DbConstants.Id + "=?", whereArgs, null, null, null);
		Provider p = null;
		if (cursor.moveToNext()) {
			p = new Provider();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String denumire = cursor.getString(DbConstants.IndexCol2);
			int idTara = cursor.getInt(DbConstants.IndexCol3);
			p.setName(denumire);
			p.setId(id);
			p.setTara(selectTara(idTara).getNume());
		}
		cursor.close();
		return p;

	}

	public List<Provider> selectProvideriDupaTara(String numeTara) {
		if(numeTara==null ||numeTara.equals("")){
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		Tara t = selectTara(numeTara);
		List<Provider> listP = new ArrayList<Provider>();
		String[] whereArgs = new String[] { String.valueOf(t.getId()) };
		Cursor cursor = bd.query(DbConstants.NumeTabelaProvideri, null,
				DbConstants.Id_tara + "=?", whereArgs, null, null, null);
		if (cursor.moveToNext()) {
			Provider p = new Provider();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String denumire = cursor.getString(DbConstants.IndexCol2);
			int idTara = cursor.getInt(DbConstants.IndexCol3);
			p.setName(denumire);
			p.setId(id);
			p.setTara(t.getNume());
			listP.add(p);
		}
		cursor.close();
		return listP;
	}

	public List<SetIP> selectIpSetDupaProvider(String numeProv) {
		if(numeProv==null ||numeProv.equals("")){
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		Provider p = selectProvider(numeProv);
		List<SetIP> listS = new ArrayList<SetIP>();
		String[] whereArgs = new String[] { String.valueOf(p.getId()) };
		Cursor cursor = bd.query(DbConstants.NumeTabelaSeturiIp, null,
				DbConstants.Id_Provider + "=?", whereArgs, null, null, null);

		if (cursor.moveToNext()) {
			SetIP si = new SetIP();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String inceput = cursor.getString(DbConstants.IndexCol2);
			String sfarsit = cursor.getString(DbConstants.IndexCol3);
			si.setFromIP(inceput);
			si.setToIP(sfarsit);
			listS.add(si);
		}
		cursor.close();
		return listS;
	}

	public Provider selectProvider(String numeProvider) {
		if(numeProvider==null ||numeProvider.equals("")){
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		String[] whereArgs = new String[] { numeProvider };
		Cursor cursor = bd.query(DbConstants.NumeTabelaProvideri, null,
				DbConstants.Denumire + "=?", whereArgs, null, null, null);
		Provider p = null;
		if (cursor.moveToNext()) {
			p = new Provider();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String denumire = cursor.getString(DbConstants.IndexCol2);
			int idTara = cursor.getInt(DbConstants.IndexCol3);
			p.setName(denumire);
			p.setId(id);
			p.setTara(selectTara(idTara).getNume());
		}
		cursor.close();
		return p;

	}

	public SetIP cautaIp(String Ip) throws InvalidIpDataFormat {
		if(Ip==null ||Ip.equals("")){
			throw new InvalidParameterException();
		}
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		long[] ipTransformed = IpTransformer.tranformIpToNumericLong(Ip);
		String ip = IpTransformer.tranformIpToNumericString(Ip);
		String ip1;
		String ip2;
		ip1 = String.valueOf(ipTransformed[0]);
		if (ipTransformed.length == 2) {
			ip2 = String.valueOf(ipTransformed[1]);
		} else {
			ip2 = "0";
		}
		Cursor cursor = bd.query(DbConstants.NumeTabelaSeturiIp, null,
				DbConstants.Inceput1 + "<=" + ip1 + " AND "
						+ DbConstants.Sfarsit1 + ">=" + ip1 + " AND "
						+ DbConstants.Inceput2 + "<=" + ip2 + " AND "
						+ DbConstants.Sfarsit2 + ">=" + ip2, null, null, null,
				null);
		SetIP s = null;
		if (cursor.moveToNext()) {
			s = new SetIP();
			int id = cursor.getInt(DbConstants.IndexCol1);
			long inceput1 = cursor.getInt(DbConstants.IndexCol2);
			long sfarsit1 = cursor.getInt(DbConstants.IndexCol3);
			long inceput2 = cursor.getInt(DbConstants.IndexCol4);
			long sfarsit2 = cursor.getInt(DbConstants.IndexCol5);
			String tip = cursor.getString(DbConstants.IndexCol6);
			int idProv = cursor.getInt(DbConstants.IndexCol7);
			long[] start = { inceput1, inceput2 };
			long[] end = { sfarsit1, sfarsit2 };
			IpFormat format = IpFormat.Ipv4;
			if (tip.equals(IpFormat.Ipv6.toString())) {
				format = IpFormat.Ipv6;
			}
			String startAddress = IpTransformer.transformLongToIpAddress(start,
					format);
			String endAddress = IpTransformer.transformLongToIpAddress(end,
					format);
			s.setFromIP(startAddress);
			s.setToIP(endAddress);
			Provider p = selectProvider(idProv);
			s.setProvider(p);
		}
		cursor.close();
		return s;
	}

	public List<Tara> obtineToateTarile() {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		List<Tara> listaTari = new ArrayList<Tara>();
		Cursor cursor = bd.query(DbConstants.NumeTabelaTari, null, null, null,
				null, null, null);

		while (cursor.moveToNext()) {
			Tara t = new Tara();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String nume = cursor.getString(DbConstants.IndexCol2);
			String descriere = cursor.getString(DbConstants.IndexCol3);
			t.setNume(nume);
			t.setDescriere(descriere);
			listaTari.add(t);
		}

		cursor.close();

		return listaTari;

	}

	public List<Provider> obtineTotiProvideri() {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		List<Provider> listaProvideri = new ArrayList<Provider>();
		Cursor cursor = bd.query(DbConstants.NumeTabelaProvideri, null, null,
				null, null, null, null);

		while (cursor.moveToNext()) {
			Provider t = new Provider();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String nume = cursor.getString(DbConstants.IndexCol2);
			int id_tara = cursor.getInt(DbConstants.IndexCol3);
			t.setId(id);
			t.setName(nume);
			Tara ta = selectTara(id_tara);
			t.setTara(ta.getNume());
			listaProvideri.add(t);
		}

		cursor.close();

		return listaProvideri;

	}

	public List<Provider> obtineTotiProvideriTara(int idTara) {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		List<Provider> listaProvideri = new ArrayList<Provider>();
		String[] whereArgs = new String[] { String.valueOf(idTara) };
		Cursor cursor = bd.query(DbConstants.NumeTabelaProvideri, null,
				DbConstants.Id_tara + "=?", whereArgs, null, null, null);

		while (cursor.moveToNext()) {
			Provider t = new Provider();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String nume = cursor.getString(DbConstants.IndexCol2);
			int id_tara = cursor.getInt(DbConstants.IndexCol3);
			t.setId(id);
			t.setName(nume);
			Tara ta = selectTara(id_tara);
			t.setTara(ta.getNume());
			listaProvideri.add(t);
		}

		cursor.close();

		return listaProvideri;

	}

	public List<SetIP> obtineSeturiProvider(int id)  {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		List<SetIP> listaSeturi = new ArrayList<SetIP>();
		String[] whereArgs = new String[] { String.valueOf(id) };
		Cursor cursor = bd.query(DbConstants.NumeTabelaSeturiIp, null,
				DbConstants.Id_Provider + "=?", whereArgs, null, null, null);

		while (cursor.moveToNext()) {
			SetIP s = new SetIP();
			long inceput1 = cursor.getInt(DbConstants.IndexCol2);
			long sfarsit1 = cursor.getInt(DbConstants.IndexCol3);
			long inceput2 = cursor.getInt(DbConstants.IndexCol3);
			long sfarsit2 = cursor.getInt(DbConstants.IndexCol4);
			String tip = cursor.getString(DbConstants.IndexCol5);
			int idProv = cursor.getInt(DbConstants.IndexCol6);
			long[] start = { inceput1, inceput2 };
			long[] end = { sfarsit1, sfarsit2 };
			IpFormat format = IpFormat.Ipv4;
			if (tip.equals(IpFormat.Ipv6.toString())) {
				format = IpFormat.Ipv6;
			}
			String startAddress = IpTransformer.transformLongToIpAddress(start,
					format);
			String endAddress = IpTransformer.transformLongToIpAddress(end,
					format);
			s.setFromIP(startAddress);
			s.setToIP(endAddress);

			// String r=cursor.getString(DbConstants.IndexCol2);
			// String b=cursor.getString(DbConstants.IndexCol3);
			s.setFromIP(startAddress);
			s.setToIP(endAddress);
			listaSeturi.add(s);
		}

		cursor.close();

		return listaSeturi;
	}

	public ArrayList<HistoricLogIP> obtineLoguri() {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		ArrayList<HistoricLogIP> listaLogs = new ArrayList<HistoricLogIP>();
		Cursor cursor = bd.query(DbConstants.NumeTabelaLog, null, null, null,
				null, null, null);

		while (cursor.moveToNext()) {
			HistoricLogIP t = new HistoricLogIP();
			int id = cursor.getInt(DbConstants.IndexCol1);
			String ip = cursor.getString(DbConstants.IndexCol2);
			String data = cursor.getString(DbConstants.IndexCol3);
			t.setIp(ip);
			t.setData(data);
			listaLogs.add(t);
		}

		cursor.close();
		return listaLogs;

	}

	public void stergereTara(int id) {
		if (bd == null) {
			throw new InvalidParameterException("sqlDatabase is null");
		}
		bd.delete(DbConstants.NumeTabelaTari, DbConstants.Id + "=" + id, null);
	}

}
