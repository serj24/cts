package com.example.proiect_android_tari.Database;

public interface DbConstants {
 public static String SITE="Site";
 public static  String DATABASE_NAME = "DB2";
 public static  String Nume = "Nume";
 public static  String Descriere = "Descriere";
 public static  String Denumire = "Denumire";
 public static  String Inceput1 = "Inceput1";
 public static  String Sfarsit1 = "Sfarsit1";
 public static  String Inceput2 = "Inceput2";
 public static  String Sfarsit2 = "Sfarsit2";
 public static  String Ipv4 = "Ipv4";
 public static  String Ipv6 = "Ipv6";
 public static String Id = "Id";
 public static String Tip = "Tip";
 public static String Id_tara = "id_tara";
 public static String Id_Provider = "Id_provider";
 public static String NumeTabelaTari="Tari";
 public static String NumeTabelaProvideri="Provideri";
 public static String NumeTabelaSeturiIp="Seturi";
 public static String NumeTabelaLog = "Loguri";
 public static String Data = "Data";
 public static String Ip = "Ip";
 static final int IndexCol1 = 0;
 static final int IndexCol2 = 1;
 static final int IndexCol3 = 2;
 static final int IndexCol4 = 3;
 static final int IndexCol5 = 4;
 static final int IndexCol6 = 5;
 static final int IndexCol7 = 6;
}
