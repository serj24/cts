package com.example.proiect_android_tari.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DbManager extends SQLiteOpenHelper{

	public DbManager(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}
    public DbManager(Context context)
    {
     super(context,DbConstants.DATABASE_NAME,null,1);
    }
    public DbManager(Context context, String databaseName)
    {
     super(context,databaseName,null,2);
    }
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaTari+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
	           + DbConstants.Nume+ " TEXT,"
				+DbConstants.Descriere+ " TEXT)");	
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaProvideri+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
		           + DbConstants.Denumire+ " TEXT," 
		           +DbConstants.Id_tara+" INTEGER,"
				+ "FOREIGN KEY("+DbConstants.Id_tara+") REFERENCES "+DbConstants.NumeTabelaTari+"("+DbConstants.Id+")"+")");
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaSeturiIp+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
		           + DbConstants.Inceput1+ " INTEGER," 
				+ DbConstants.Sfarsit1+ " INTEGER,"
				 + DbConstants.Inceput2+ " INTEGER," 
				+ DbConstants.Sfarsit2+ " INTEGER,"
				 + DbConstants.Tip+ " TEXT," 
				+DbConstants.Id_Provider+" INTEGER,"
		         +  "FOREIGN KEY("+DbConstants.Id_Provider+") REFERENCES "+DbConstants.NumeTabelaTari+"("+DbConstants.Id+")"+")");
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaLog+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
	           + DbConstants.Ip+ " TEXT,"
				+DbConstants.Data+ " TEXT)");	
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		 db.execSQL("DROP TABLE IF EXISTS " + DbConstants.NumeTabelaTari);
		 db.execSQL("DROP TABLE IF EXISTS " + DbConstants.NumeTabelaProvideri);
		 db.execSQL("DROP TABLE IF EXISTS " + DbConstants.NumeTabelaSeturiIp);
		 db.execSQL("DROP TABLE IF EXISTS " + DbConstants.NumeTabelaLog);
	      onCreate(db);
		
	}

}
