package com.example.proiect_android_tari.Grafic;

import java.util.ArrayList;

import com.example.proiect_android_tari.ActivitateGrafic;

import android.R.color;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class ControlGrafic extends View{

	Paint paint;

	ArrayList<Integer> Listay =new  ArrayList<Integer>();
	ArrayList<String> Listax =new ArrayList<String>();
	ArrayList<Paint> culoriBare = new ArrayList<Paint>();



	public ControlGrafic(Context context,AttributeSet set){
		super(context,set);
		initializare();
	}
	
	public ControlGrafic(Context context) {
		super(context);
		initializare();
	}

	protected void onDraw (Canvas canvas){
		paint =new Paint();
		Paint paint2 =new Paint();
		paint.setColor(Color.BLUE);
		paint.setStrokeWidth(10);
		paint.setTextSize(3);
		paint2.setColor(Color.RED);
		paint2.setTextSize(10);
		int max=0;
		int n;
		if(ActivitateGrafic.Histograma.size()>10)n=10;
		else n=ActivitateGrafic.Histograma.size();
		for(int i=0;i<n;i++)
		{
			if(ActivitateGrafic.Histograma.get(i).getNumar()>max)max=ActivitateGrafic.Histograma.get(i).getNumar();
		}
		int latime = ((getWidth()-10)-50)/n;
		int unit=0;
		if(max!=0){
			unit=(getHeight()-50)/max;
		}
		
		
		for(int i=0;i<n;i++)
		{
			int d;
			if(i%2==0) d=20;
			else d=40;
			canvas.drawText(ActivitateGrafic.Histograma.get(i).getNume(),latime*i+latime/2,getHeight()-d, paint);
			canvas.drawText(String.valueOf(ActivitateGrafic.Histograma.get(i).getNumar()),latime*i+latime/2,30, paint2);
			canvas.drawRect(latime*i+latime/2, getHeight()-60-ActivitateGrafic.Histograma.get(i).getNumar()*unit, latime*(i+1)+latime/2-5, getHeight()-60, paint);
		}
		/*//canvas.drawRect(5,5, getWidth()-5, getHeight()-10, paint);
		int max = this.getMax();
		int latime = ((getWidth()-10)-50)/Listay.size();
		int unit=(getHeight()-50)/max;
		for(int i =0; i<Listay.size();i++){
			//canvas.drawRect(latime*i+10,(getHeight()-50)-unit*Listay.get(i),latime*i+10+latime,unit*Listay.get(i),culoriBare.get(i));
			canvas.drawText(Listax.get(i),latime*i+latime/2,getHeight()-40, paint);
			Rect r=new Rect(latime*i+10,(getHeight()-50)-unit*Listay.get(i),latime*i+10+latime,unit*Listay.get(i));
			//Rect r=new Rect(10,10,10,10);
			//Rect r2=new Rect(10,50,120,this.getHeight()-50);
			//Rect r3=new Rect(60,this.getHeight()-200,120,200);
			canvas.drawRect(r,paint);
			//canvas.drawRect(r2,paint);
			//canvas.drawRect(r3,paint);
		}*/
	}
	
	public int getMax(){
		
		int max=Listay.get(0);
		for(int i=0;i<Listay.size();i++){
			if(max < Listay.get(i))
				max=Listay.get(i);
		}
		return max;
	}
	
	public void initializare(){
		paint = new Paint();
	    paint.setColor(Color.GREEN);
	    paint.setStyle(Style.STROKE);
	    paint.setStrokeWidth(10);
	    
		
	    Listax.add("sport");
	    Listax.add("politica");
	    Listax.add("monden");
	    Listax.add("medicina");
	    
	    Listay.add(20);
	    Listay.add(53);
	    Listay.add(19);
	    Listay.add(38);
	    
	    
		for(int i=0;i<Listay.size();i++){
			Paint p = new Paint();
			culoriBare.add(p);
		}
		culoriBare.get(0).setColor(Color.BLUE);
		culoriBare.get(1).setColor(Color.RED);
		culoriBare.get(2).setColor(Color.GREEN);
		culoriBare.get(3).setColor(Color.MAGENTA);
		
	    
	}
}
