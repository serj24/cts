package com.example.proiect_android_tari;

import java.security.InvalidParameterException;
import java.util.Date;

public class HistoricLogIP {

	String ip;
	String data;
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		if(ip==null){
			throw new InvalidParameterException();
		}
		this.ip = ip;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		if(data==null){
			throw new InvalidParameterException();
		}
		this.data = data;
	}

	public HistoricLogIP(String ip, String data) {
		super();
		if(ip==null){
			throw new InvalidParameterException();
		}
		if(data==null){
			throw new InvalidParameterException();
		}
		this.ip = ip;
		this.data = data;
	}

	public HistoricLogIP() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Ip:" + ip + ", Date=" +data;//+ data.getYear()+"\\"+data.getMonth()+"\\"+data.getDay()+" "+data.getHours()+":"+data.getMinutes()+":"+data.getSeconds() ;
	}
		
	
}
