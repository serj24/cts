package com.example.proiect_android_tari;

import java.util.ArrayList;
import java.util.List;

public class ListaTari {
public static List<Tara> ListaTari=new ArrayList<Tara>();
public static void Add(Tara tara)
{
 ListaTari.add(tara);
}
public static Boolean ExistaTara(String nume)
{
	for(int i=0;i<ListaTari.size();i++)
		  if(ListaTari.get(i).getNume().equals(nume)) return true;
	return false;
}
public static Tara GetTara(String nume)
{
  for(int i=0;i<ListaTari.size();i++)
	  if(ListaTari.get(i).getNume().equals(nume)) return ListaTari.get(i);
  return null;
}
public static long size()
{
	return ListaTari.size();
}
public static Tara get(int i)
{
   return ListaTari.get(i);
}
}
