package com.example.proiect_android_tari;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


import org.w3c.dom.Document;

import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

public class PreluareIpuri extends AsyncTask<String, Void, ArrayList<Provider>> {

	@Override
	protected ArrayList<Provider> doInBackground(String... param) {
		// TODO Auto-generated method stub
		
		ArrayList<Provider> provideri = new ArrayList<Provider>();
		URL url = null;
		HttpURLConnection con = null;
		StringBuilder sb = new StringBuilder();
		
		try {
			url = new URL(param[0]);
			Log.v("test",url.toString());
			con = (HttpURLConnection) url.openConnection();
			InputStream is = con.getInputStream();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader (is));
			String linie;
			
			linie = reader.readLine();
			
			int ok = 0;
			
			while (linie != null) {
				sb.append(linie);
				if(ok ==1 )
				{
					   
					   String tr[] = linie.split("<tr>");
					   for(String s : tr){
						  
						   String td[] = s.split("<td>");
						   
						   for(String s2 : td){
							   //Log.v("tst",s2);
							 
						   }
						 
						   if(td.length > 1){
						   td[1] = (td[1].split(" "))[0];	   
						   td[2] = (td[2].split(" "))[0];
						   SetIP set = new SetIP(td[1],td[2],td[4]);
						   Provider prov = new Provider(td[5], param[1]);
						   prov.AddIpRange(set);
						   provideri.add(prov);
								   
						   }
					   }
				}
				if(linie.lastIndexOf("Owner") != -1 )
					ok = 1;
				else 
					ok = 0;
				linie = reader.readLine();
			}
			
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		finally{
			if(con != null)
				con.disconnect();
		}
		
		
		
		return provideri;
	}
	


}
