package com.example.proiect_android_tari;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Provider {
public List<SetIP> ListaIp;
public String Name;
public String Tara;
public int Id;
public Provider(String name, String tara) {
	super();
	if(name==null || name.equals("")){
		throw new InvalidParameterException();
	}
	if(tara==null || tara.equals("")){
		throw new InvalidParameterException();
	}
	ListaIp = new ArrayList<SetIP>();
	Name = name;
	Tara = tara;
}
public Provider() {
	ListaIp = new ArrayList<SetIP>();
}
public void AddIpRange(SetIP setIP)
{
	if(setIP == null){
		throw new InvalidParameterException();
	}
   ListaIp.add(setIP);
}
public List<SetIP> getListaIp() {
	return ListaIp;
}
public void setListaIp(List<SetIP> listaIp) {
	ListaIp = listaIp;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public String getTara() {
	return Tara;
}
public void setTara(String tara) {
	if(tara==null || tara.equals("")){
		throw new InvalidParameterException();
	}
	Tara = tara;
}
public void setId(int id) {
	Id=id;
}
public int getId() {
	return Id;
}
public  Boolean ExistaSetIP(SetIP set)
{
	if(set == null){
		throw new InvalidParameterException();
	}
	for(int i=0;i<ListaIp.size();i++)
		  if(ListaIp.get(i).getFromIP().equals(set.getFromIP())&&ListaIp.get(i).getToIP().equals(set.getToIP())) return true;
	return false;
}
public double SearchForIP(String IP) throws InvalidIpDataFormat
{
	for(int i=0;i<ListaIp.size();i++)
		if(ListaIp.get(i).ContainsIP(IP)) return i;
		return -1;
}
public boolean equals(Provider p) {
	if(p==null){
		 throw new InvalidParameterException();
	}
    return this.Name.equals(p.getName()) && 
    		this.Tara.equals(p.getTara()) && 
    		this.getListaIp().equals(p.getListaIp());
}
public boolean equals(Object o) {
	if(o==null){
		 throw new InvalidParameterException();
	}
	if(o instanceof Provider){
	Provider p =(Provider)o;
    return equals(p);
	}
	return false;
}
}
