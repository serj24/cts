package com.example.proiect_android_tari.Retea;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidParameterException;
import java.util.ArrayList;


import com.example.proiect_android_tari.Tara;

public class HttpReader {

	
	public static String getResponse(String Url){
		if(Url == null || Url.equals("")){
			throw new InvalidParameterException();
		}
		String linie="";
		HttpURLConnection connection = null;
		StringBuilder sb = new StringBuilder();
		try {
			URL url  = new URL(Url);
			connection = (HttpURLConnection) url.openConnection();
			
			//preluare raspuns
			InputStream is = connection.getInputStream();
			//prelucrare continut
			BufferedReader reader = new BufferedReader(new InputStreamReader (is));
			
			linie = reader.readLine();
			while (linie != null) {
				sb.append(linie);
				linie = reader.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return sb.toString();
	}
}
