package com.example.proiect_android_tari.Retea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.example.proiect_android_tari.Tara;

import android.os.AsyncTask;
import android.util.Log;

public class PreluareTari extends AsyncTask <String, Void,String> {
	@Override
	protected String doInBackground(String... param) {
		//accesul la retea
		return  HttpReader.getResponse(param[0]);
	
	}
	
}
