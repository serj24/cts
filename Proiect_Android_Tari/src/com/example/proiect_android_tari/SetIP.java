package com.example.proiect_android_tari;

import java.security.InvalidParameterException;
import java.util.Date;

import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.IpTransformer;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class SetIP {
	private String FromIP;
	private String ToIP;
	private String Date;
	private Provider Prov;

	public SetIP(String fromIP, String toIP, String date) {
		if (fromIP == null || fromIP.equals("") || toIP == null
				|| toIP.equals("") || date == null || date.equals("")) {
			throw new InvalidParameterException();
		}
		FromIP = fromIP;
		ToIP = toIP;
		Date = date;
		Prov = null;
	}

	public SetIP() {
		Prov = null;
	}

	@Override
	public String toString() {
		return FromIP + " " + ToIP;
	}

	public String getFromIP() {
		return FromIP;
	}

	public void setFromIP(String fromIP) {
		FromIP = fromIP;
	}

	public String getToIP() {
		return ToIP;
	}

	public void setToIP(String toIP) {
      if(toIP==null || toIP.equals("")){
    	  throw new InvalidParameterException();
      }
		ToIP = toIP;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public Provider getProvider() {
		return Prov;
	}

	public void setProvider(Provider prov) {
		 if(prov == null){
	    	  throw new InvalidParameterException();
	      }
		Prov = prov;
	}

	public Boolean ContainsIP(String Ip) {
		try {
			long Ip1 = IpTransformer.tranformIpToNumericLong(FromIP)[0];
			long Ip2 = IpTransformer.tranformIpToNumericLong(ToIP)[0];
			long IpSearch = IpTransformer.tranformIpToNumericLong(Ip)[0];
			return IpSearch >= Ip1 && IpSearch <= Ip2;
		} catch (InvalidIpDataFormat e) {
			return false;
		}
	}

	public boolean equals(SetIP ip) {
		if(ip==null){
			 throw new InvalidParameterException();
		}
		return this.FromIP.equals(ip.getFromIP())
				&& this.ToIP.equals(ip.getToIP());
	}

	public boolean equals(Object o) {
		if(o == null){
			 throw new InvalidParameterException();
		}
		if (o instanceof SetIP) {
			return equals((SetIP) o);
		}
		return false;
	}
}
