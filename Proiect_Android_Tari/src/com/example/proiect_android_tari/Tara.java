package com.example.proiect_android_tari;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

public class Tara {
 private String Nume;
 private String Descriere;
 private int Id;
 private List<Provider> ListaProvideri;
@Override
public String toString() {
	String nume= "Nume=" + Nume +" Provideri:";
	for(int i=0;i<ListaProvideri.size();i++)
		nume+=ListaProvideri.get(i).Name+",";
	return nume;
}
public Tara(String nume) {
	if(nume==null || nume.equals("")){
		throw new InvalidParameterException();
	}
	Nume = nume;
	Descriere="";
	ListaProvideri=new ArrayList<Provider>();
}
public Tara(String nume, String descriere) {
	if(nume==null || descriere==null || nume.equals("")){
		throw new InvalidParameterException();
	}
	Nume = nume;
	Descriere=descriere;
	ListaProvideri=new ArrayList<Provider>();
}
public Tara(){
	Nume = "";
	Descriere= "";
	ListaProvideri=new ArrayList<Provider>();
}
public void AddProvider(Provider provider)
{
	if(provider==null){
	 throw new InvalidParameterException();
	}
  ListaProvideri.add(provider);
}
public String getNume() {
	return Nume;
}
public void setId(int id) {
	Id=id;
}
public int getId() {
	return Id;
}
public void setNume(String nume) {
	Nume = nume;
}
public String getDescriere() {
	return Descriere;
}
public void setDescriere(String descriere) {
	Descriere=descriere ;
}
 
public Boolean ExistaProvider(String nume)
{
	for(int i=0;i<ListaProvideri.size();i++)
		  if(ListaProvideri.get(i).getName().equals(nume)) return true;
	return false;
}
public List<Provider> getProvideri()
{
	return ListaProvideri;
}
public Provider GetProvider(String nume)
{
  for(int i=0;i<ListaProvideri.size();i++)
	  if(ListaProvideri.get(i).getName().equals(nume)) return ListaProvideri.get(i);
  return null;
}
public boolean equals(Tara t) {
	if(t==null){
		 throw new InvalidParameterException();
	}
    return this.Nume.equals(t.getNume()) && 
    		this.getDescriere().equals(t.getDescriere()) && 
    		this.getProvideri().equals(t.getProvideri());
}
public boolean equals(Object o) {
	if(o==null){
		 throw new InvalidParameterException();
	}
	if(o instanceof Tara){
		return equals((Tara)o);
	}
    return false;
}
}
