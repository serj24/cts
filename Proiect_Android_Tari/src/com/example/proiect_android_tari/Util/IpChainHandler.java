package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

//Chain of Responsability
public abstract class IpChainHandler {
		protected IpChainHandler succesor = null;
		protected IpValidator validator;
		public IpChainHandler(){
			validator=new IpValidator();
		}
		public void setSuccessor(IpChainHandler nextHnadler){
			if(nextHnadler==null){
			throw new InvalidParameterException();
			}
			succesor = nextHnadler;
		}

  public abstract String transformIpToNumericString(String address) throws InvalidIpDataFormat;
  public abstract long[] transformIpToNumericLong(String address) throws InvalidIpDataFormat;
}
