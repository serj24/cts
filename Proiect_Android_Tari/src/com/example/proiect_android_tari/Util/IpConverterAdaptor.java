package com.example.proiect_android_tari.Util;

import java.lang.reflect.Array;
import java.security.InvalidParameterException;
import java.util.List;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class IpConverterAdaptor implements IpConvertor{

	IpConvertorIpv4 converter;
	public IpConverterAdaptor(IpConvertorIpv4 converter){
		if(converter==null){
			throw new InvalidParameterException();
		}
	this.converter=converter;
	}
	
	@Override
	public long[] ipToLong(String ipAddress) throws InvalidIpDataFormat {
		long res= converter.ipToLong(ipAddress);
		long[] longs = {res};
		 return longs;
	}

	@Override
	public String longToIp(long[] ip) throws InvalidIpDataFormat {
		if(ip==null){
			throw new InvalidParameterException();
		}
		return converter.longToIp(ip[0]);
	}
}
