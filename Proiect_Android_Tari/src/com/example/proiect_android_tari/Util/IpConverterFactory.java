package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

public class IpConverterFactory {
public static IpConvertor createIpConvertor(IpFormat format){
	switch(format){
	case Ipv4:
		return new IpConverterAdaptor(new IpConvertorIpv4());
	case Ipv6:
		return new IpConvertorIpv6();
	default:
		throw new InvalidParameterException();
	}
}
}
