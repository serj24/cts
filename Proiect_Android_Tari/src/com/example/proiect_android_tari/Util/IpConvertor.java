package com.example.proiect_android_tari.Util;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public interface IpConvertor {
	public  long[] ipToLong(String ipAddress)throws InvalidIpDataFormat;
	public  String longToIp(long []ip)throws InvalidIpDataFormat;
}
