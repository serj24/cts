package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class IpConvertorIpv4{
	
	public long ipToLong(String ipAddress) throws InvalidIpDataFormat {
		if(ipAddress==null){
			throw new InvalidParameterException();
		}
		long result = 0;
		try{ 
		String[] ipAddressInArray = ipAddress.split("\\.");
		if(ipAddressInArray.length != 4){
			throw new InvalidIpDataFormat();
		}
		for (int i = 0; i < ipAddressInArray.length; i++) {

			int power = 3 - i;
			int ip = Integer.parseInt(ipAddressInArray[i]);
			result += ip * Math.pow(256, power);
		}
	    }catch(Exception e){
		throw new InvalidIpDataFormat();
		}
		return result;
	}
	public String longToIp(long ip) throws InvalidIpDataFormat {
		try{
		StringBuilder result = new StringBuilder(15);

		for (int i = 0; i < 4; i++) {

			result.insert(0,Long.toString(ip & 0xff));

			if (i < 3) {
				result.insert(0,'.');
			}

			ip = ip >> 8;
		}
		return result.toString();
		}catch(Exception e){
			throw new InvalidIpDataFormat();
			}
		}	
}
