package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

import android.os.Handler;

public class IpTransformer {

	public static String tranformIpToNumericString(String ip)
			throws InvalidIpDataFormat {
		IpChainHandler ipHandler4 = new Ipv4Handler();
		IpChainHandler ipHandler6 = new Ipv6Handler();
		ipHandler4.setSuccessor(ipHandler6);
		return ipHandler4.transformIpToNumericString(ip);
	}

	public static long[] tranformIpToNumericLong(String ip)
			throws InvalidIpDataFormat {
		IpChainHandler ipHandler4 = new Ipv4Handler();
		IpChainHandler ipHandler6 = new Ipv6Handler();
		ipHandler4.setSuccessor(ipHandler6);
		return ipHandler4.transformIpToNumericLong(ip);
	}

	public static String transformLongToIpAddress(long[] ip, IpFormat format) {
		try {
			switch (format) {
			case Ipv4:
				return new IpConverterAdaptor(new IpConvertorIpv4())
						.longToIp(ip);
			case Ipv6:
				return new IpConvertorIpv6().longToIp(ip);
			default:
				throw new InvalidParameterException();
			}
		} catch (Exception e) {
			return null;
		}

	}
}
