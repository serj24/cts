package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

//State
public class IpValidator {
IIpFormatValidator validator;

public IpValidator(){
 validator = new Ipv4FormatValidator();
}

public void setIpFormat(IpFormat format){
	if(format==null){
		throw new InvalidParameterException();
	}
 this.validator=IpValidatorFactory.createValidator(format);
}
 public boolean validate(String ipAddress){
	 return this.validator.isValidIpFormat(ipAddress);
 }
 public static boolean validateV4AndV6 (String ipAddress){
	 if(ipAddress==null){
		 throw new InvalidParameterException();
	 }
	 IpValidator validator = new IpValidator();
	 validator.setIpFormat(IpFormat.Ipv4);
	 boolean isIpv4=validator.validate(ipAddress);
	 validator.setIpFormat(IpFormat.Ipv6);
	 boolean isIpv6=validator.validate(ipAddress);
	 return isIpv4 || isIpv6;
 }
}
