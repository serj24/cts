package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

public class IpValidatorFactory {

	public static IIpFormatValidator createValidator(IpFormat format){
		if(format==null){
			throw new InvalidParameterException();
		}
		
	 switch(format){
	 case Ipv4:
		 return new Ipv4FormatValidator();
	 case Ipv6:
		 return new Ipv6FormatValidator();
	 default: 
        throw new InvalidParameterException();
	 }
	 
	}
}
