package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv4FormatValidator implements IIpFormatValidator {

	@Override
	public boolean isValidIpFormat(String ipAddress) {
		if (ipAddress == null) {
			throw new InvalidParameterException();
		}
	    final String patternString = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(ipAddress);
		return matcher.matches();
	}
}
