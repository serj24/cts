package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv4Handler extends IpChainHandler{

	@Override
	public String transformIpToNumericString(String ipAddress) throws InvalidIpDataFormat {
		if(ipAddress==null){
			throw new InvalidParameterException();
		}
		validator.setIpFormat(IpFormat.Ipv4);
		if(validator.validate(ipAddress)){
		IpConvertorIpv4 convertor = new IpConvertorIpv4();
		return String.valueOf(convertor.ipToLong(ipAddress));
		}else{
			if(succesor!=null){
			  return succesor.transformIpToNumericString(ipAddress);
			}else{
				throw new InvalidIpDataFormat();
			}
		}
	}

	@Override
	public long[] transformIpToNumericLong(String ipAddress) throws InvalidIpDataFormat{
	if(ipAddress==null){
		throw new InvalidParameterException();
	}
	validator.setIpFormat(IpFormat.Ipv4);
	if(validator.validate(ipAddress)){
	IpConvertorIpv4 convertor = new IpConvertorIpv4();
	IpConverterAdaptor adaptor =new IpConverterAdaptor(convertor);
	return adaptor.ipToLong(ipAddress);
	}else{
		if(succesor!=null){
		  return succesor.transformIpToNumericLong(ipAddress);
		}else{
			throw new InvalidIpDataFormat();
		}
	}
	}

}
