package com.example.proiect_android_tari.Util;

import java.security.InvalidParameterException;

import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv6Handler extends IpChainHandler {

	@Override
	public String transformIpToNumericString(String ipAddress)
			throws InvalidIpDataFormat {
		if (ipAddress == null) {
			throw new InvalidParameterException();
		}
		validator.setIpFormat(IpFormat.Ipv6);
		if (validator.validate(ipAddress)) {
			IpConvertor convertor = new IpConvertorIpv6();
			long[] result = convertor.ipToLong(ipAddress);
			return String.valueOf(result[0]);//+ "," + String.valueOf(result[1]);
		} else {
			if (succesor != null) {
				return succesor.transformIpToNumericString(ipAddress);
			} else {
				throw new InvalidIpDataFormat();
			}
		}
	}

	@Override
	public long[] transformIpToNumericLong(String ipAddress) throws InvalidIpDataFormat {
		if (ipAddress == null) {
			throw new InvalidParameterException();
		}
		validator.setIpFormat(IpFormat.Ipv6);
		if (validator.validate(ipAddress)) {
			IpConvertor convertor = new IpConvertorIpv6();
			long[] result = convertor.ipToLong(ipAddress);
			return result;
		} else {
			if (succesor != null) {
				return succesor.transformIpToNumericLong(ipAddress);
			} else {
				throw new InvalidIpDataFormat();
			}
		}
	}
}
