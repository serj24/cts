package com.example.proiect_android_tari.Util;


	public class Pair<X, Y> { 
		  public final X x; 
		  public final Y y; 
		  public Pair(X x, Y y) { 
		    this.x = x; 
		    this.y = y; 
		  } 
		  public X first(){
			  return x;
		  }
		  public Y second(){
			  return y;
		  }
	} 


