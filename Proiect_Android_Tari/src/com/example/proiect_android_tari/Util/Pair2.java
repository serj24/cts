package com.example.proiect_android_tari.Util;



public class Pair2<X, Y,Z> { 
	  public final X x; 
	  public final Y y; 
	  public final Z z; 
	  public Pair2(X x, Y y, Z z) { 
	    this.x = x; 
	    this.y = y; 
	    this.z =z;
	  } 
	  public X first(){
		  return x;
	  }
	  public Y second(){
		  return y;
	  }
	  public Z third(){
		  return z;
	  }
} 


