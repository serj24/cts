package com.example.proiect_android_tari.exceptions;

public class AlreadyExistingDataException extends Exception{
	public AlreadyExistingDataException(){
		super("This data already exist in database!");
	}

}
