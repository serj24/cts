package com.example.proiect_android_tari;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.Database.DbConstants;
import com.example.proiect_android_tari.Database.DbManager;
import com.example.proiect_android_tari.Grafic.Histogram;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.CountryNotFoundException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ActivityUnitTestCase;
import android.widget.Toast;
import static org.mockito.Mockito.*;

public class ActivitateGraficTest extends ActivityInstrumentationTestCase2<ActivitateGrafic>{

	@Mock
	private DbManager dbManager;
	private DAOTari daoTari;
	private SQLiteDatabase db;
	Context context;
	
	public  ActivitateGraficTest() {
		super(ActivitateGrafic.class);
	}

	
	@Before
	public void setUp() {
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
		Intent intent=new Intent(getInstrumentation().getTargetContext(), ActivitateGrafic.class);
	    intent.putExtra("Tara","Romania");
	    setActivityIntent(intent);
	}

	
	@Test
	public void testActivitateGrafic() {
		DAOTari daoTari=mock(DAOTari.class);
		Tara tara=mock(Tara.class);
		when(tara.getNume()).thenReturn("Romania");
		when(tara.getId()).thenReturn(1);
		when(daoTari.selectTara(anyString())).thenReturn(tara);
		List<Provider>listaProvideri=new ArrayList<Provider>();
		when(daoTari.obtineTotiProvideriTara(1)).thenReturn(listaProvideri);

	    List<Histogram> lista=null;
		try {
			lista = getActivity().getHistogram(daoTari);
		} catch (CountryNotFoundException e) {
			fail("expected no expection but found CountryNotFoundException");
		}
		
	}

}
