package com.example.proiect_android_tari.Database;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import java_unit_test.Ipv4ConverterJavaTest;
import junit.framework.Assert;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.proiect_android_tari.ActivitatePrincipala;
import com.example.proiect_android_tari.HistoricLogIP;
import com.example.proiect_android_tari.Provider;
import com.example.proiect_android_tari.SetIP;
import com.example.proiect_android_tari.Tara;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityUnitTestCase;
import android.test.InstrumentationTestCase;
import android.util.Log;
import static org.mockito.Mockito.*;

public class DAOTariIpTest extends InstrumentationTestCase {

	// @Mock
	private Context context;
	@Mock
	private DbManager dbManager;
	private DAOTari daoTari;
	private SQLiteDatabase db;

	public DAOTariIpTest() {
		super();
	}

	public void initializeDatabase(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaTari+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
	           + DbConstants.Nume+ " TEXT,"
				+DbConstants.Descriere+ " TEXT)");	
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaProvideri+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
		           + DbConstants.Denumire+ " TEXT," 
		           +DbConstants.Id_tara+" INTEGER,"
				+ "FOREIGN KEY("+DbConstants.Id_tara+") REFERENCES "+DbConstants.NumeTabelaTari+"("+DbConstants.Id+")"+")");
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaSeturiIp+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
		           + DbConstants.Inceput1+ " INTEGER," 
				+ DbConstants.Sfarsit1+ " INTEGER,"
				 + DbConstants.Inceput2+ " INTEGER," 
				+ DbConstants.Sfarsit2+ " INTEGER,"
				 + DbConstants.Tip+ " TEXT," 
				+DbConstants.Id_Provider+" INTEGER,"
		         +  "FOREIGN KEY("+DbConstants.Id_Provider+") REFERENCES "+DbConstants.NumeTabelaTari+"("+DbConstants.Id+")"+")");
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaLog+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
	           + DbConstants.Ip+ " TEXT,"
				+DbConstants.Data+ " TEXT)");	
	}

	@Before
	public void setUp() {
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
		MockitoAnnotations.initMocks(this);
		context = getInstrumentation().getTargetContext();
		db = context.openOrCreateDatabase("test3", 1, null);
		when(dbManager.getWritableDatabase()).thenReturn(db);
		when(dbManager.getReadableDatabase()).thenReturn(db);
		initializeDatabase(db);
		// daoTari = new DAOTari(getInstrumentation().getTargetContext());
		daoTari = new DAOTari(dbManager);
	}

	@After
	public void tearDown() {
		boolean deleteDatabase = context.deleteDatabase("test3");
	}

	@Test
	public void testAdaugaInvalidRange() throws InvalidIpDataFormat {
		daoTari.open();
		long expectedResult=-1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaRange("Provider", "Tara","172.16.190.0", "172.16.128.255");
			fail("Expected = InvalidIpSetException");
		} catch (AlreadyExistingDataException e) {
			fail();
		} catch (InvalidIpSetException e) {
		}
		assertEquals(expectedResult, actualResult);
		
		daoTari.close();
	}
	
	@Test
	public void testAdaugaInvalidIpAdresseFirst() throws InvalidIpSetException {
		daoTari.open();
		long expectedResult=-1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaRange("Provider", "Tara","172.16.190.0.12", "172.16.128.255");
			fail("Expected = InvalidIpDataFormat");
		} catch (AlreadyExistingDataException e) {
			fail();
		} catch (InvalidIpDataFormat e) {
		} 
		assertEquals(expectedResult, actualResult);
		
		daoTari.close();
	}
	
	@Test
	public void testAdaugaInvalidIpAdresseSecond() throws InvalidIpSetException {
		daoTari.open();
		long expectedResult=-1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaRange("Provider", "Tara","172.16.190.0", "172.16.128");
			fail("Expected = InvalidIpDataFormat");
		} catch (AlreadyExistingDataException e) {
			fail();
		} catch (InvalidIpDataFormat e) {
		} 
		assertEquals(expectedResult, actualResult);
		
		daoTari.close();
	}
	
	@Test
	public void testAdaugaRangeIpv4() throws InvalidIpDataFormat {
		daoTari.open();
		long expectedResult=1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaRange("Provider", "Tara","172.16.1.0", "172.16.128.255");
		} catch (AlreadyExistingDataException e) {
			fail();
		} catch (InvalidIpSetException e) {
			fail();
		} 
		assertEquals(expectedResult, actualResult);
		
		SetIP setIp = daoTari.cautaIp("172.16.2.23");
		assertNotNull(setIp);
		assertEquals(setIp.getFromIP(), "172.16.1.0");
		assertEquals(setIp.getToIP(), "172.16.128.255");
		daoTari.close();
	}
	@Test
	public void testAdaugaRangeIpv6() throws InvalidIpDataFormat {
		daoTari.open();
		long expectedResult=1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaRange("Provider", "Tara", "3ffe:1900:4545:3:200:f8ff:a221:67cf", "3ffe:1900:4545:3:200:f8ff:af21:cfcf");
		} catch (AlreadyExistingDataException e) {
			fail();
		} catch (InvalidIpSetException e) {
			fail();
		} 
		assertEquals(expectedResult, actualResult);
		
		SetIP setIp = daoTari.cautaIp("3ffe:1900:4545:3:200:f8ff:a231:68cf");
		assertNotNull(setIp);
		daoTari.close();
	}
	@Test
	public void testSelectSetIp() throws InvalidIpDataFormat {
		daoTari.open();
		try {
			daoTari.adaugaRange("Provider", "Tara","172.16.1.0", "172.16.128.255");
			daoTari.adaugaRange("Provider2", "Tara","128.116.1.0", "128.116.128.255");
			daoTari.adaugaRange("Provider", "Tara","254.160.1.0", "254.160.128.255");
		} catch (AlreadyExistingDataException e) {
			fail();
		} catch (InvalidIpSetException e) {
			fail();
		} 
		List<SetIP> listaIpuri=daoTari.obtineSeturiProvider(1);
		assertEquals(listaIpuri.size(), 2);
		assertEquals(listaIpuri.get(0).getFromIP(), "172.16.1.0");
		assertEquals(listaIpuri.get(0).getToIP(), "172.16.128.255");
		assertEquals(listaIpuri.get(1).getFromIP(), "254.160.1.0");
		assertEquals(listaIpuri.get(1).getToIP(), "254.160.128.255");
		
		daoTari.close();
	}
	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest("testAdaugaInvalidIpAdresseFirs"));
		suite.addTest(new Ipv4ConverterJavaTest("testAdaugaInvalidIpAdresseSecond"));
		runner.doRun(suite);
	}
}
