package com.example.proiect_android_tari.Database;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import java_unit_test.Ipv4ConverterJavaTest;
import junit.framework.Assert;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.proiect_android_tari.ActivitatePrincipala;
import com.example.proiect_android_tari.HistoricLogIP;
import com.example.proiect_android_tari.Provider;
import com.example.proiect_android_tari.SetIP;
import com.example.proiect_android_tari.Tara;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityUnitTestCase;
import android.test.InstrumentationTestCase;
import android.util.Log;
import static org.mockito.Mockito.*;

public class DAOTariProvideriTest extends InstrumentationTestCase {

	// @Mock
	private Context context;
	@Mock
	private DbManager dbManager;
	private DAOTari daoTari;
	private SQLiteDatabase db;

	public DAOTariProvideriTest() {
		super();
	}

	public void initializeDatabase(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaTari+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
	           + DbConstants.Nume+ " TEXT,"
				+DbConstants.Descriere+ " TEXT)");	
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaProvideri+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
		           + DbConstants.Denumire+ " TEXT," 
		           +DbConstants.Id_tara+" INTEGER,"
				+ "FOREIGN KEY("+DbConstants.Id_tara+") REFERENCES "+DbConstants.NumeTabelaTari+"("+DbConstants.Id+")"+")");
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaSeturiIp+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
		           + DbConstants.Inceput1+ " INTEGER," 
				+ DbConstants.Sfarsit1+ " INTEGER,"
				 + DbConstants.Inceput2+ " INTEGER," 
				+ DbConstants.Sfarsit2+ " INTEGER,"
				 + DbConstants.Tip+ " TEXT," 
				+DbConstants.Id_Provider+" INTEGER,"
		         +  "FOREIGN KEY("+DbConstants.Id_Provider+") REFERENCES "+DbConstants.NumeTabelaTari+"("+DbConstants.Id+")"+")");
		db.execSQL("CREATE TABLE "+DbConstants.NumeTabelaLog+" (" +
				"Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
	           + DbConstants.Ip+ " TEXT,"
				+DbConstants.Data+ " TEXT)");	
	}

	@Before
	public void setUp() {
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
		MockitoAnnotations.initMocks(this);
		context = getInstrumentation().getTargetContext();
		db = context.openOrCreateDatabase("test3", 1, null);
		when(dbManager.getWritableDatabase()).thenReturn(db);
		when(dbManager.getReadableDatabase()).thenReturn(db);
		initializeDatabase(db);
		// daoTari = new DAOTari(getInstrumentation().getTargetContext());
		daoTari = new DAOTari(dbManager);
	}

	@After
	public void tearDown() {
		boolean deleteDatabase = context.deleteDatabase("test3");
	}

	@Test
	public void testAdaugaProviderNull() {
		daoTari.open();
		long expectedResult = 1;
		long actualResult = -1;
		try {
			actualResult = daoTari.adaugaProvider(null, null);
			fail(" expected InvalidParameterException");
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		} catch (InvalidParameterException e) {
		}
		daoTari.close();
	}
	
	@Test
	public void testAdaugaProvider() {
		daoTari.open();
		long expectedResult=1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaProvider("Test_Provider", "Tara");
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}
		assertEquals(expectedResult, actualResult);	
	}

	@Test
	public void testAdaugaProviderTwoTimes() {
		daoTari.open();
		long expectedResult=-1;
		long actualResult=-1;
		try {
			daoTari.adaugaProvider("Test_Provider", "Tara");
			actualResult = daoTari.adaugaProvider("Test_Provider", "Tara");
			fail("AlreadyExistingDataException expected but was no exception");
		} catch (AlreadyExistingDataException e) {
		}
		assertEquals(expectedResult, actualResult);
		Tara actualTara = daoTari.selectTara("Tara");
		assertNotNull(actualTara);
		Provider actualProvider = daoTari.selectProvider("Test_Provider");
		assertNotNull(actualProvider);
		assertEquals(actualProvider.getName(), "Test_Provider");
	}
	@Test
	public void testSelectProvider() {
		daoTari.open();
		long expectedResult=1;
		long actualResult=-1;
		try {
			actualResult = daoTari.adaugaProvider("Test_Provider", "Tara");
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}
		assertEquals(expectedResult, actualResult);
		Tara actualTara = daoTari.selectTara("Tara");
		assertNotNull(actualTara);
		Provider actualProvider = daoTari.selectProvider("Test_Provider");
		assertNotNull(actualProvider);
		assertEquals(actualProvider.getName(), "Test_Provider");
		
		Provider actualProvider2=daoTari.selectProvider(actualProvider.getId());
		assertEquals(actualProvider, actualProvider2);
		daoTari.close();
	}
	@Test
	public void testObtineProvideri() {
		daoTari.open();
		try {
			 daoTari.adaugaProvider("Test_Provider", "Tara");
			 daoTari.adaugaProvider("Test_Provider2", "Tara2");
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}
		
		List<Provider> listaProvideri=daoTari.obtineTotiProvideri();
		assertEquals(listaProvideri.size(), 2);
		assertEquals(listaProvideri.get(0).getName(), "Test_Provider");
		assertEquals(listaProvideri.get(1).getName(), "Test_Provider2");
		assertEquals(listaProvideri.get(0).getTara(), "Tara");
		assertEquals(listaProvideri.get(1).getTara(), "Tara2");
	}
	@Test
	public void testObtineProvideriDupaTara() {
		daoTari.open();
		try {
			 daoTari.adaugaProvider("Test_Provider", "Tara");
			 daoTari.adaugaProvider("Test_Provider2", "Tara2");
			 daoTari.adaugaProvider("Test_Provider3", "Tara");
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}
		
		List<Provider> listaProvideri=daoTari.obtineTotiProvideriTara(1);
		assertEquals(listaProvideri.size(), 2);
		assertEquals(listaProvideri.get(0).getName(), "Test_Provider");
		assertEquals(listaProvideri.get(1).getName(), "Test_Provider3");
		assertEquals(listaProvideri.get(0).getTara(), "Tara");
		assertEquals(listaProvideri.get(0).getTara(), "Tara");
	}
	
	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest("testAdaugaInvalidIpAdresseFirs"));
		suite.addTest(new Ipv4ConverterJavaTest("testAdaugaInvalidIpAdresseSecond"));
		runner.doRun(suite);
	}
}
