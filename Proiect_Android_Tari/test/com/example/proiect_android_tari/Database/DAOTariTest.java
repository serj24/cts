package com.example.proiect_android_tari.Database;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import java_unit_test.Ipv4ConverterJavaTest;
import junit.framework.Assert;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.proiect_android_tari.ActivitatePrincipala;
import com.example.proiect_android_tari.HistoricLogIP;
import com.example.proiect_android_tari.Provider;
import com.example.proiect_android_tari.SetIP;
import com.example.proiect_android_tari.Tara;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityUnitTestCase;
import android.test.InstrumentationTestCase;
import android.util.Log;
import static org.mockito.Mockito.*;

public class DAOTariTest extends InstrumentationTestCase {

	// @Mock
	private Context context;
	@Mock
	private DbManager dbManager;
	private DAOTari daoTari;
	private SQLiteDatabase db;

	public DAOTariTest() {
		super();
	}

	public void initializeDatabase(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + DbConstants.NumeTabelaTari + " ("
				+ "Id INTEGER PRIMARY KEY AUTOINCREMENT  , " + DbConstants.Nume
				+ " TEXT," + DbConstants.Descriere + " TEXT)");
		db.execSQL("CREATE TABLE " + DbConstants.NumeTabelaProvideri + " ("
				+ "Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
				+ DbConstants.Denumire + " TEXT," + DbConstants.Id_tara
				+ " INTEGER," + "FOREIGN KEY(" + DbConstants.Id_tara
				+ ") REFERENCES " + DbConstants.NumeTabelaTari + "("
				+ DbConstants.Id + ")" + ")");
		db.execSQL("CREATE TABLE " + DbConstants.NumeTabelaSeturiIp + " ("
				+ "Id INTEGER PRIMARY KEY AUTOINCREMENT  , "
				+ DbConstants.Inceput1 + " INTEGER," + DbConstants.Sfarsit1
				+ " INTEGER," + DbConstants.Inceput2 + " INTEGER,"
				+ DbConstants.Sfarsit2 + " INTEGER," + DbConstants.Tip
				+ " TEXT," + DbConstants.Id_Provider + " INTEGER,"
				+ "FOREIGN KEY(" + DbConstants.Id_Provider + ") REFERENCES "
				+ DbConstants.NumeTabelaTari + "(" + DbConstants.Id + ")" + ")");
		db.execSQL("CREATE TABLE " + DbConstants.NumeTabelaLog + " ("
				+ "Id INTEGER PRIMARY KEY AUTOINCREMENT  , " + DbConstants.Ip
				+ " TEXT," + DbConstants.Data + " TEXT)");
	}

	@Before
	public void setUp() {
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
		MockitoAnnotations.initMocks(this);
		context = getInstrumentation().getTargetContext();
		db = context.openOrCreateDatabase("test3", 1, null);
		when(dbManager.getWritableDatabase()).thenReturn(db);
		when(dbManager.getReadableDatabase()).thenReturn(db);
		initializeDatabase(db);
		// daoTari = new DAOTari(getInstrumentation().getTargetContext());
		daoTari = new DAOTari(dbManager);
	}

	@After
	public void tearDown() {
		boolean deleteDatabase = context.deleteDatabase("test3");
	}

	@Test()
	public void testConstructorEmptyStringForDbName() {
		try {
			DAOTari dao = new DAOTari(context, "");
			fail("expected=invalidParameterExp");
		} catch (InvalidParameterException e) {
		}
	}

	@Test()
	public void testConstructorNULLForDbName() {
		try {
			DAOTari dao = new DAOTari(context, null);
			fail("expected=invalidParameterExp");
		} catch (InvalidParameterException e) {
		}
	}

	@Test()
	public void testConstructorNULLForContext() {
		try {
			DAOTari dao = new DAOTari(null, "db");
			fail("expected=invalidParameterExp");
		} catch (InvalidParameterException e) {
		}
	}

	// @Rule
	// public ExpectedException thrown = ExpectedException.none();

	@Test
	public void testNullBdClosedDb() {
		daoTari = new DAOTari(context, "test");
		try {
			daoTari.close();
			fail("expected=InvalidParameterException");
		} catch (InvalidParameterException e) {
			assertTrue(e.getMessage().equals("sqlDatabase is null"));
		}
	}

	@Test
	public void testAlreadyClosedDb() {
		daoTari.open();
		daoTari.close();
		try {
			daoTari.close();
			fail("expected=UnsupportedOperationException");
		} catch (UnsupportedOperationException e) {
			assertTrue(e.getMessage().equals("sqlDatabase already closed"));
		}

	}

	@Test
	public void testOpenDb() {
		daoTari = new DAOTari(context, "testOpenDb");
		daoTari.open();
		String dataBases[] = context.databaseList();
		boolean existDb = false;
		for (int i = 0; i < dataBases.length; i++) {
			if (dataBases[i].equals("testOpenDb")) {
				existDb = true;
			}
		}
		assertTrue(existDb);
		boolean deleteDatabase = context.deleteDatabase("testOpenDb");
	}

	@Test
	public void testAdaugaLog() {
		daoTari.open();
		HistoricLogIP logIp = mock(HistoricLogIP.class);
		String Ip = "122.34.123.123";
		when(logIp.getIp()).thenReturn(Ip);
		when(logIp.getData()).thenReturn(
				Calendar.getInstance().getTime().toString());
		long expectedResult = 1;
		long actualResult = daoTari.adaugaLog(logIp);
		assertEquals(expectedResult, actualResult);
		daoTari.close();
	}

	@Test
	public void testOptineLog() {
		daoTari.open();
		HistoricLogIP logIp = mock(HistoricLogIP.class);
		String Ip = "122.34.123.123";
		when(logIp.getIp()).thenReturn(Ip);
		when(logIp.getData()).thenReturn(
				Calendar.getInstance().getTime().toString());
		long expectedResult = 1;
		long actualResult = daoTari.adaugaLog(logIp);
		assertEquals(expectedResult, actualResult);
		List<HistoricLogIP> list = daoTari.obtineLoguri();
		assertTrue(list.size() == 1);
		HistoricLogIP actualLog = list.get(0);
		assertEquals(logIp.getData(), actualLog.getData());
		assertEquals(logIp.getIp(), actualLog.getIp());

		daoTari.close();
	}

	@Test
	public void testAdaugaTara() {
		daoTari.open();
		Tara tara = mock(Tara.class);
		when(tara.getDescriere()).thenReturn("Test_Descriere");
		when(tara.getNume()).thenReturn("Test_Nume");
		long expectedResult = 1;
		long actualResult = -1;
		try {
			actualResult = daoTari.adaugaTara(tara);
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}
		assertEquals(expectedResult, actualResult);
		daoTari.close();
	}

	@Test
	public void testAdaugaTaraNull() {
		daoTari.open();
		long expectedResult = 1;
		long actualResult = -1;
		try {
			actualResult = daoTari.adaugaTara(null);
			fail(" expected InvalidParameterException");
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		} catch (InvalidParameterException e) {
		}
		daoTari.close();
	}

	@Test
	public void testAdaugaTaraTwoTimes() {
		daoTari.open();
		Tara tara = mock(Tara.class);
		when(tara.getDescriere()).thenReturn("Test_Descriere");
		when(tara.getNume()).thenReturn("Test_Nume");
		long expectedResult = -1;
		long actualResult = -1;
		try {
			daoTari.adaugaTara(tara);
			daoTari.adaugaTara(tara);
			fail("AlreadyExistingDataException expected");
		} catch (AlreadyExistingDataException e) {
		}
		assertEquals(expectedResult, actualResult);
		daoTari.close();
	}

	@Test
	public void testSelectTara() {
		daoTari.open();
		Tara tara = mock(Tara.class);
		when(tara.getDescriere()).thenReturn("Test_Descriere");
		when(tara.getNume()).thenReturn("Test_Nume");
		long expectedResult = 1;
		long actualResult = -1;
		try {
			actualResult = daoTari.adaugaTara(tara);
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}
		assertEquals(expectedResult, actualResult);
		Tara actualTara = daoTari.selectTara("Test_Nume");
		assertEquals(actualTara.getNume(), "Test_Nume");
		assertEquals(actualTara.getDescriere(), "Test_Descriere");

		Tara actualTara2 = daoTari.selectTara(actualTara.getId());
		assertEquals(actualTara, actualTara2);

		List<Tara> listaTari = daoTari.obtineToateTarile();
		assertTrue(listaTari.size() == 1);
		assertEquals(listaTari.get(0), actualTara);

		daoTari.close();
	}

	@Test
	public void testObtineTari() {
		daoTari.open();
		Tara tara = mock(Tara.class);
		when(tara.getDescriere()).thenReturn("Test_Descriere");
		when(tara.getNume()).thenReturn("Test_Nume");
		Tara tara2 = mock(Tara.class);
		when(tara2.getDescriere()).thenReturn("Test_Descriere2");
		when(tara2.getNume()).thenReturn("Test_Nume2");
		try {
			daoTari.adaugaTara(tara);
			daoTari.adaugaTara(tara2);
		} catch (AlreadyExistingDataException e) {
			fail("no exception expected but was AlreadyExistingDataException");
		}

		List<Tara> listaTari = daoTari.obtineToateTarile();
		assertTrue(listaTari.size() == 2);
		assertEquals(listaTari.get(0), tara);
		assertEquals(listaTari.get(1), tara2);

		daoTari.close();
	}

	@Test
	public void testStergeTara() {
		daoTari.open();
		Tara tara = mock(Tara.class);
		when(tara.getDescriere()).thenReturn("Test_Descriere");
		when(tara.getNume()).thenReturn("Test_Nume");
		long expectedResult = 1;
		long actualResult = -1;
		try {
			actualResult = daoTari.adaugaTara(tara);
		} catch (AlreadyExistingDataException e) {
		}

		assertEquals(expectedResult, actualResult);
		Tara actualTara = daoTari.selectTara("Test_Nume");
		assertEquals(actualTara.getNume(), "Test_Nume");
		assertEquals(actualTara.getDescriere(), "Test_Descriere");
		assertNotNull(actualTara.getId());
		daoTari.stergereTara(actualTara.getId());

		Tara actualTara2 = daoTari.selectTara("Test_Nume");
		assertNull(actualTara2);
		daoTari.close();
	}

	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest(
				"testAdaugaInvalidIpAdresseFirs"));
		suite.addTest(new Ipv4ConverterJavaTest(
				"testAdaugaInvalidIpAdresseSecond"));
		runner.doRun(suite);
	}
}
