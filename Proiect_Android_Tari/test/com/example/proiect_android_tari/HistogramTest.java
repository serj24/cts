package com.example.proiect_android_tari;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.Database.DbConstants;
import com.example.proiect_android_tari.Database.DbManager;
import com.example.proiect_android_tari.Grafic.Histogram;
import com.example.proiect_android_tari.exceptions.AlreadyExistingDataException;
import com.example.proiect_android_tari.exceptions.CountryNotFoundException;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;
import com.example.proiect_android_tari.exceptions.InvalidIpSetException;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.test.ActivityUnitTestCase;
import android.widget.Toast;
import static org.mockito.Mockito.*;

public class HistogramTest extends ActivityUnitTestCase{

	@Mock
	private DbManager dbManager;
	private DAOTari daoTari;
	private SQLiteDatabase db;
	Context context;
	
	public HistogramTest(Class activityClass) {
		super(activityClass);
	}
	public HistogramTest() {
		super(ActivitateGrafic.class);
	}


	@Test
	public void testActivitateGrafic() {
		
		Intent intent=new Intent(getInstrumentation().getTargetContext(), ActivitateGrafic.class);
	    intent.putExtra("Tara","Rzxdewf");
	    startActivity(intent, null, null);
	    ActivitateGrafic activitateGrafic = (ActivitateGrafic) getActivity();
	    assertEquals(activitateGrafic.getIntent().getStringExtra("Tara"),"Rzxdewf");
	    List<Histogram> lista=null;
		try {
			lista = activitateGrafic.getHistogram(new DAOTari(getInstrumentation().getTargetContext()));
			fail("expected CountryNotFoundException but found no expection");
		} catch (CountryNotFoundException e) {
			
		}
	}

}
