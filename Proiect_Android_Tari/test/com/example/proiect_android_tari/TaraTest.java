package com.example.proiect_android_tari;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import android.test.InstrumentationTestCase;
import static org.mockito.Mockito.*;

public class TaraTest extends InstrumentationTestCase {

	@Before
	public void setUp(){
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
	}
	@Test
	public void testConstructorNullParam() {
		try {
			Tara t = new Tara(null);
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}
	@Test
	public void testConstructorNullParam2() {
		try {
			Tara t = new Tara("tara", null);
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}
	@Test
	public void testConstructorNullParam3() {
		try {
			Tara t = new Tara(null,null);
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}
	@Test
	public void testConstructorParam1() {
		try {
			Tara t = new Tara("tara");
			assertEquals("tara", t.getNume());
		} catch (InvalidParameterException e) {
			fail("expected = no expection");
		}
	}

	@Test
	public void testConstructorParam2() {
		try {
			Tara t = new Tara("Romania","Descriere");
			assertEquals("Romania", t.getNume());
			assertEquals("Descriere", t.getDescriere());
		} catch (InvalidParameterException e) {
			fail("expected=no exception");
		}
	}
	@Test
	public void testSetMethods() {
		try {
			Tara t = new Tara();
			t.setNume("Romania");
			t.setDescriere("Descriere");
			assertEquals("Romania", t.getNume());
			assertEquals("Descriere", t.getDescriere());
		} catch (InvalidParameterException e) {
			fail("expected=no exception");
		}
	}
	@Test
	public void testConstructorEmptyString() {
		try {
			Tara t = new Tara("");
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}
	
	@Test
	public void testSet(){
	   Tara t=new Tara();
	   t.setNume("Romania");
	   t.setDescriere("descriere");
	   assertEquals(t.getNume(), "Romania");
	   assertEquals(t.getDescriere(), "descriere");
	}
	
	@Test
	public void testEquals(){
	   Tara t=new Tara();
	   t.setNume("Romania");
	   t.setDescriere("descriere");
	   Tara t2=new Tara("Romania","descriere");
	   assertEquals(t,t2);
	}
	@Test
	public void testEqualsNull() {
		try {
			 Tara t=new Tara("Romania","descriere");
			 boolean a=t.equals(null);
			 fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}
	@Test
	public void testAdaugaProviderNull() {
		try {
			Tara t = new Tara();
			t.AddProvider(null);
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}
	@Test
	public void testAdaugaProvider() {
		try {
			Tara t = new Tara("Tara");
			Provider actualProvider=mock(Provider.class);
			when(actualProvider.getName()).thenReturn("p");
			when(actualProvider.getTara()).thenReturn("Tara");
			t.AddProvider(actualProvider);
			assertEquals(t.GetProvider("p"), actualProvider);
			assertNotNull(t.getProvideri());
			assertEquals(1, t.getProvideri().size());
			assertEquals(t.getProvideri().get(0), actualProvider);
		} catch (InvalidParameterException e) {
			fail("expected = no exception");
		}
	}

}
