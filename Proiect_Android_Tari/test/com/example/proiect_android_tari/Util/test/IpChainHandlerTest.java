package com.example.proiect_android_tari.Util.test;


import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import android.test.InstrumentationTestCase;
import android.view.WindowManager.InvalidDisplayException;

import com.example.proiect_android_tari.Util.IpChainHandler;
import com.example.proiect_android_tari.Util.Ipv4Handler;
import com.example.proiect_android_tari.Util.Ipv6Handler;
import com.example.proiect_android_tari.Util.Pair;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

import junit.framework.TestCase;

public class IpChainHandlerTest extends  InstrumentationTestCase {

	static List<Pair<String, Long>> listCorrectIpv4Set;
	static List<Pair<String, Long>> listCorrectIpv6Set;
	static String[] incorrectIpV4Address = { "1222.34.123.123",
			"567.34.123.123", "1222.34.523.123" };
	static String[] incorrectIpV6Address = {
			"3ffe:1900:4545:3:200:f8ff:fe21:67cf:1234",
			"3ffe:1900:4545:3:200:f8ff", ":3:200:f8ff:fe21:67cf" };

	@Before
	public void setUp() {
		//initialize mockito
		System.setProperty("dexmaker.dexcache", getInstrumentation()
				.getTargetContext().getCacheDir().getPath());
		//initializare lista de date
		listCorrectIpv4Set = new ArrayList<Pair<String, Long>>();
		listCorrectIpv4Set.add(new Pair("122.34.123.32", 2049080096L));
		listCorrectIpv4Set.add(new Pair("122.34.123.123", 2049080187L));
		listCorrectIpv4Set.add(new Pair("90.34.23.123", 1512183675L));
		listCorrectIpv4Set.add(new Pair("10.34.123.123", 170031995L));

		listCorrectIpv6Set = new ArrayList<Pair<String, Long>>();
		listCorrectIpv6Set.add(new Pair("3ffe:1900:4545:3:200:f8ff:fe21:67cf",
				144388966439806927L));
		listCorrectIpv6Set.add(new Pair("FE80:0:0:0:202:B3FF:FE1E:8329",
				144876050090722089L));
		listCorrectIpv6Set.add(new Pair("FE80:0:0:0:8329:8329:8329:8329",
				-8995514566378421463L));
		listCorrectIpv6Set.add(new Pair("2001:db8:0:1:1:1:1:1",
				281479271743489L));
	}

	@Test
	public void testChainHandlerIpv4First() throws InvalidIpDataFormat{
		String ipAddress="3ffe:1900:4545:3:200:f8ff:fe21:67cf";
		long []ipLong={144388966439806927L,4611150557426810883L};
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv4.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericLong(ipAddress)).thenReturn(ipLong);
		long []result = handlerIpv4.transformIpToNumericLong(ipAddress);
		assertEquals(ipLong[0], result[0]);
		assertEquals(ipLong[1], result[1]);
		assertEquals(result.length,2);
	}
	@Test
	public void testChainHandlerIpv4FirstException() throws InvalidIpDataFormat{
		String ipAddress="3ffe:1900:4545:3:200:f8ff:fe21:67cf";
		//long ipLong=144388966439806927L;
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv4.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericLong(ipAddress)).thenThrow(new InvalidIpDataFormat());
		try {
			handlerIpv4.transformIpToNumericLong(ipAddress);
			fail("expected=InvalidIpDataFormat.class");
		} catch (InvalidIpDataFormat e) {
		}
	}
	
	@Test
	public void testChainHandlerIpv6First() throws InvalidIpDataFormat{
		String ipAddress="122.34.123.32";
		long []ipLong={2049080096L};
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv6.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericLong(ipAddress)).thenReturn(ipLong);
		long []result=handlerIpv6.transformIpToNumericLong(ipAddress);
		assertEquals(result.length, 1);
		assertEquals(ipLong[0], result[0]);
	}
	@Test
	public void testChainHandlerIpv6FirstException() throws InvalidIpDataFormat{
		String ipAddress="122.34.123.32";
		long ipLong=2049080096L;
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv6.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericLong(ipAddress)).thenThrow( new InvalidIpDataFormat());
		try {
			handlerIpv6.transformIpToNumericLong(ipAddress);
			fail("expected=InvalidIpDataFormat.class");
		} catch (InvalidIpDataFormat e) {
		}
	}
	
/// transformIpToNumericString
	@Test
	public void testChainHandlerIpv4FirstTransformIpToNumericString() throws InvalidIpDataFormat{
		String ipAddress="3ffe:1900:4545:3:200:f8ff:fe21:67cf";
		String ipLong=String.valueOf(144388966439806927L);
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv4.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericString(ipAddress)).thenReturn(ipLong);
		assertEquals(handlerIpv4.transformIpToNumericString(ipAddress), ipLong);
	}
	@Test
	public void testChainHandlerIpv4FirstExceptionTransformIpToNumericString() throws InvalidIpDataFormat{
		String ipAddress="3ffe:1900:4545:3:200:f8ff:fe21:67cf";
		String ipLong=String.valueOf(144388966439806927L);
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv4.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericString(ipAddress)).thenThrow(new InvalidIpDataFormat());
		try {
			handlerIpv4.transformIpToNumericString(ipAddress);
			fail("expected=InvalidIpDataFormat.class");
		} catch (InvalidIpDataFormat e) {
		}
	}
	
	@Test
	public void testChainHandlerIpv6FirstTransformIpToNumericString() throws InvalidIpDataFormat{
		String ipAddress="122.34.123.32";
		String ipLong=String.valueOf(2049080096L);
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv6.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericString(ipAddress)).thenReturn(ipLong);
		assertEquals(handlerIpv6.transformIpToNumericString(ipAddress), ipLong);
	}
	@Test
	public void testChainHandlerIpv6FirstExceptionTransformIpToNumericString() throws InvalidIpDataFormat{
		String ipAddress="122.34.123.32";
		String ipLong=String.valueOf(2049080096L);
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		IpChainHandler secondHandler = mock(IpChainHandler.class);
		handlerIpv6.setSuccessor(secondHandler);
		when(secondHandler.transformIpToNumericString(ipAddress)).thenThrow(new InvalidIpDataFormat());
		try {
			handlerIpv6.transformIpToNumericString(ipAddress);
			fail("expected=InvalidIpDataFormat.class");
		} catch (InvalidIpDataFormat e) {
		}
	}
	//
	
	@Test
	public void testIpToNumericLongIpv4HandlerCorrectAddresses() {
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		for (int i = 0; i < listCorrectIpv4Set.size(); i++) {
			try {
				long []actualResultLong = handlerIpv4
						.transformIpToNumericLong(listCorrectIpv4Set.get(i)
								.first());
				assertEquals(actualResultLong.length, 1);
				assertTrue(listCorrectIpv4Set.get(i).second() == actualResultLong[0]);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToNumericLongIpv4HandlerIncorrectAddresses() {
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			try {
				long []actualResultLong = handlerIpv4
						.transformIpToNumericLong(incorrectIpV4Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testIpToNumericLongIpv6HandlerCorrectAddresses() {
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		for (int i = 0; i < listCorrectIpv6Set.size(); i++) {
			try {
				long []actualResultLong = handlerIpv6
						.transformIpToNumericLong(listCorrectIpv6Set.get(i)
								.first());
				assertEquals(actualResultLong.length, 2);
				assertTrue(listCorrectIpv6Set.get(i).second() == actualResultLong[0]);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToNumericLongIpv6HandlerIncorrectAddresses() {
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			try {
				long []actualResultLong = handlerIpv6
						.transformIpToNumericLong(incorrectIpV6Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testIpToNumericLongChainHandlerCorrectAddressesIpv4First() {
		IpChainHandler mainHandler = new Ipv4Handler();
		IpChainHandler secondHandler = new Ipv6Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < listCorrectIpv4Set.size(); i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(listCorrectIpv4Set.get(i)
								.first());
				assertEquals(actualResultLong.length, 1);
				assertTrue(listCorrectIpv4Set.get(i).second() == actualResultLong[0]);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}

		for (int i = 0; i < listCorrectIpv6Set.size(); i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(listCorrectIpv6Set.get(i)
								.first());
				assertEquals(actualResultLong.length, 2);
				assertTrue(listCorrectIpv6Set.get(i).second() == actualResultLong[0]);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test
	public void testIpToNumericLongChainHandlerCorrectAddressesIpv6First() {
		IpChainHandler mainHandler = new Ipv6Handler();
		IpChainHandler secondHandler = new Ipv4Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < listCorrectIpv4Set.size(); i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(listCorrectIpv4Set.get(i)
								.first());
				assertEquals(actualResultLong.length, 1);
				assertTrue(listCorrectIpv4Set.get(i).second() == actualResultLong[0]);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}

		for (int i = 0; i < listCorrectIpv6Set.size(); i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(listCorrectIpv6Set.get(i)
								.first());
				assertTrue(listCorrectIpv6Set.get(i).second() == actualResultLong[0]);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test
	public void testChainHandlerIncorrectAddressesIpv4First() {
		IpChainHandler mainHandler = new Ipv4Handler();
		IpChainHandler secondHandler = new Ipv6Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(incorrectIpV4Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}

		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(incorrectIpV6Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testChainHandlerIncorrectAddressesIpv6First() {
		IpChainHandler mainHandler = new Ipv6Handler();
		IpChainHandler secondHandler = new Ipv4Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			try {
				long[] actualResultLong = mainHandler
						.transformIpToNumericLong(incorrectIpV4Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}

		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			try {
				long[] actualResultLong = mainHandler
						.transformIpToNumericLong(incorrectIpV6Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}
	
	////////
	@Test
	public void testIpToNumericStringIpv4HandlerCorrectAddresses() {
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		for (int i = 0; i < listCorrectIpv4Set.size(); i++) {
			try {
				String actualResultString = handlerIpv4
						.transformIpToNumericString(listCorrectIpv4Set.get(i)
								.first());
				assertEquals(listCorrectIpv4Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToNumericStringIpv4HandlerIncorrectAddresses() {
		IpChainHandler handlerIpv4 = new Ipv4Handler();
		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			try {
				String actualResultLong = handlerIpv4
						.transformIpToNumericString(incorrectIpV4Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testIpToNumericStringIpv6HandlerCorrectAddresses() {
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		for (int i = 0; i < listCorrectIpv6Set.size(); i++) {
			try {
				String actualResultString = handlerIpv6
						.transformIpToNumericString(listCorrectIpv6Set.get(i)
								.first());
				assertEquals(listCorrectIpv6Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToNumericStringIpv6HandlerIncorrectAddresses() {
		IpChainHandler handlerIpv6 = new Ipv6Handler();
		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			try {
				String actualResultLong = handlerIpv6
						.transformIpToNumericString(incorrectIpV6Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testIpToNumericStringChainHandlerCorrectAddressesIpv4First() {
		IpChainHandler mainHandler = new Ipv4Handler();
		IpChainHandler secondHandler = new Ipv6Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < listCorrectIpv4Set.size(); i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(listCorrectIpv4Set.get(i)
								.first());
				assertEquals(listCorrectIpv4Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}

		for (int i = 0; i < listCorrectIpv6Set.size(); i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(listCorrectIpv6Set.get(i)
								.first());
				assertEquals(listCorrectIpv6Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test
	public void testIpToNumericStringChainHandlerCorrectAddressesIpv6First() {
		IpChainHandler mainHandler = new Ipv6Handler();
		IpChainHandler secondHandler = new Ipv4Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < listCorrectIpv4Set.size(); i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(listCorrectIpv4Set.get(i)
								.first());
				assertEquals(listCorrectIpv4Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}

		for (int i = 0; i < listCorrectIpv6Set.size(); i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(listCorrectIpv6Set.get(i)
								.first());
				assertEquals(listCorrectIpv6Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception");
			}
		}
	}

	@Test
	public void testChainHandlerIpToNumericStringIncorrectAddressesIpv4First() {
		IpChainHandler mainHandler = new Ipv4Handler();
		IpChainHandler secondHandler = new Ipv6Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(incorrectIpV4Address[i]);
				assertEquals(listCorrectIpv4Set.get(i).second().toString(), actualResultString);
			} catch (InvalidIpDataFormat e) {
			}
		}

		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			try {
				long []actualResultLong = mainHandler
						.transformIpToNumericLong(incorrectIpV6Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testChainHandlerIpToNumericStringIncorrectAddressesIpv6First() {
		IpChainHandler mainHandler = new Ipv6Handler();
		IpChainHandler secondHandler = new Ipv4Handler();
		mainHandler.setSuccessor(secondHandler);

		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(incorrectIpV4Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}

		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			try {
				String actualResultString = mainHandler
						.transformIpToNumericString(incorrectIpV6Address[i]);
				fail("expected=InvalidIpDataFormat.class");
			} catch (InvalidIpDataFormat e) {
			}
		}
	}
}
