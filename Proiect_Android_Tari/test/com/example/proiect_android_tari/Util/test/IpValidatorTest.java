package com.example.proiect_android_tari.Util.test;

import java_unit_test.Ipv4ConverterJavaTest;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.Before;
import org.junit.Test;

import com.example.proiect_android_tari.Util.IpFormat;
import com.example.proiect_android_tari.Util.IpValidator;

public class IpValidatorTest extends TestCase {

	String[] correctIpV4Address = { "122.34.12.12", "1.115.12.1", "1.23.5.23",
			"1.34.12.113" };
	String[] correctIpV6Address = { "3ffe:1900:4545:3:200:f8ff:fe21:67cf",
			"FE80:0:0:0:202:B3FF:FE1E:8329" };
	String[] incorrectIpV4Address = { "1222.34.123.123", "567.34.123.123",
			"1222.34.523.123" };
	String[] incorrectIpV6Address = {
			"3ffe:1900:4545:3:200:f8ff:fe21:67cf:1234",
			"3ffe:1900:4545:3:200:f8ff", ":3:200:f8ff:fe21:67cf" };

	public IpValidatorTest(String name) {
		super(name);
	}
	@Before
	public void setUp() {

	}

	@Test
	public void testCorrectDefault() {
		IpValidator validator = new IpValidator();
		for (int i = 0; i < correctIpV4Address.length; i++) {
			assertTrue(validator.validate(correctIpV4Address[i]));
		}
	}

	@Test
	public void testCorrectIpv4Set() {
		IpValidator validator = new IpValidator();
		validator.setIpFormat(IpFormat.Ipv4);
		for (int i = 0; i < correctIpV4Address.length; i++) {
			assertTrue(validator.validate(correctIpV4Address[i]));
		}
	}

	@Test
	public void testIpv6Default() {
		IpValidator validator = new IpValidator();
		for (int i = 0; i < correctIpV6Address.length; i++) {
			assertEquals(false, validator.validate(correctIpV6Address[i]));
		}
	}

	@Test
	public void testIpv4WidthIpv6Set() {
		IpValidator validator = new IpValidator();
		validator.setIpFormat(IpFormat.Ipv4);
		for (int i = 0; i < correctIpV6Address.length; i++) {
			assertEquals(false, validator.validate(correctIpV6Address[i]));
		}
	}

	@Test
	public void testInvalidFormatDefault() {
		IpValidator validator = new IpValidator();
		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			assertEquals(false, validator.validate(incorrectIpV4Address[i]));
		}
	}

	@Test
	public void testInvalidFormatIpv4() {
		IpValidator validator = new IpValidator();
		validator.setIpFormat(IpFormat.Ipv4);
		for (int i = 0; i < incorrectIpV4Address.length; i++) {
			assertEquals(false, validator.validate(incorrectIpV4Address[i]));
		}
	}

	@Test
	public void testCorrectIpv6Set() {
		IpValidator validator = new IpValidator();
		validator.setIpFormat(IpFormat.Ipv6);
		for (int i = 0; i < correctIpV6Address.length; i++) {
			assertTrue(validator.validate(correctIpV6Address[i]));
		}
	}

	@Test
	public void testIpv6WidthIpv4Set() {
		IpValidator validator = new IpValidator();
		validator.setIpFormat(IpFormat.Ipv6);
		for (int i = 0; i < correctIpV4Address.length; i++) {
			assertEquals(false, validator.validate(correctIpV4Address[i]));
		}
	}

	@Test
	public void testInvalidFormatIpv6() {
		IpValidator validator = new IpValidator();
		validator.setIpFormat(IpFormat.Ipv6);
		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			assertEquals(false, validator.validate(incorrectIpV6Address[i]));
		}
	}

	@Test
	public void testStaticCorrectIpv4() {
		for (int i = 0; i < correctIpV4Address.length; i++) {
			assertTrue(IpValidator.validateV4AndV6(correctIpV4Address[i]));
		}
	}

	@Test
	public void testStaticCorrectIpv6() {
		for (int i = 0; i < correctIpV4Address.length; i++) {
			assertTrue(IpValidator.validateV4AndV6(correctIpV4Address[i]));
		}
	}

	@Test
	public void testStaticIncorrectIpv4() {
		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			assertTrue(!IpValidator.validateV4AndV6(incorrectIpV6Address[i]));
		}
	}

	@Test
	public void testStaticIncorrectIpv6() {
		for (int i = 0; i < incorrectIpV6Address.length; i++) {
			assertTrue(!IpValidator.validateV4AndV6(incorrectIpV6Address[i]));
		}
	}
	
	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new IpValidatorTest("testCorrectIpv4Set"));
		suite.addTest(new IpValidatorTest("testStaticIncorrectIpv4"));
		runner.doRun(suite);
	}
}
