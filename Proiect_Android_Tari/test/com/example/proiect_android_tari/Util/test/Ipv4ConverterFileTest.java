package com.example.proiect_android_tari.Util.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import java_unit_test.Ipv4ConverterJavaTest;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import android.test.InstrumentationTestCase;

import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.IpConvertorIpv6;
import com.example.proiect_android_tari.Util.Pair;
import com.example.proiect_android_tari.Util.test.Ipv4ConverterTest;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv4ConverterFileTest extends InstrumentationTestCase {

	static List<Pair<String, Long>> listCorrectSet;
	static List<Pair<String, Long>> listIncorrectLongValues;
	static List<String> listIncorrectIpv4Address;
	IpConvertorIpv4 convertor;

	Reader readerCorrectSet;
	BufferedReader brCorrectSet;
	Reader readerIncorrectLSet;
	BufferedReader brIncorrectLSet;
	Reader readerIncorrectASet;
	BufferedReader brIncorrectASet;

	@Before
	public void setUp() throws IOException {
		readerCorrectSet = new InputStreamReader(getInstrumentation()
				.getTargetContext().getAssets().open("Ipv4Set.txt"));
		brCorrectSet = new BufferedReader(readerCorrectSet);
		readerIncorrectLSet = new InputStreamReader(getInstrumentation()
				.getTargetContext().getAssets()
				.open("IpIv4IncorectLongValues.txt"));
		brIncorrectLSet = new BufferedReader(readerIncorrectLSet);
		readerIncorrectASet = new InputStreamReader(getInstrumentation()
				.getTargetContext().getAssets()
				.open("Ipv4IncorectAddresses.txt"));
		brIncorrectASet = new BufferedReader(readerIncorrectASet);
		convertor = new IpConvertorIpv4();

		listCorrectSet = getCorrectSet();
		listIncorrectLongValues = getIncorrectLongValues();
		listIncorrectIpv4Address = getIncorrectIpAddress();

	}

	@After
	public void tearDown() throws IOException {
		brCorrectSet.close();
		readerCorrectSet.close();
		brIncorrectASet.close();
		readerIncorrectASet.close();
		brIncorrectLSet.close();
		readerIncorrectLSet.close();
	}

	private ArrayList<Pair<String, Long>> getCorrectSet() throws IOException {
		ArrayList<Pair<String, Long>> list = new ArrayList<Pair<String, Long>>();
		String line = brCorrectSet.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			list.add(new Pair(set[0], Long.parseLong(set[1])));
			line = brCorrectSet.readLine();
		}
		return list;
	}

	private ArrayList<Pair<String, Long>> getIncorrectLongValues()
			throws IOException {
		ArrayList<Pair<String, Long>> list = new ArrayList<Pair<String, Long>>();
		String line = brIncorrectLSet.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			list.add(new Pair(set[0], Long.parseLong(set[1])));
			line = brIncorrectLSet.readLine();
		}
		return list;
	}

	private ArrayList<String> getIncorrectIpAddress() throws IOException {
		ArrayList<String> list = new ArrayList<String>();
		String line = brIncorrectASet.readLine();
		while (line != null) {
			list.add(line);
			line = brIncorrectASet.readLine();
		}
		return list;
	}

	@Test
	public void testIpToLongCorrect() {
		for (Pair<String, Long> pair : listCorrectSet) {
			long actualResult;
			try {
				actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				assertEquals(actualResult, (long) pair.second());
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testLongToIpCorrect() {
		for (Pair<String, Long> pair : listCorrectSet) {
			String actualResult;
			try {
				actualResult = convertor.longToIp(pair.second());
				assertNotNull(actualResult);
				assertEquals(actualResult, pair.first());
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testIpToLongIncorrect() {
		for (Pair<String, Long> pair : listIncorrectLongValues) {
			long actualResult;
			try {
				actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				assertTrue(actualResult != (long) pair.second());
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testLongToIpIncorrect() {
		for (Pair<String, Long> pair : listIncorrectLongValues) {
			String actualResult;
			try {
				actualResult = convertor.longToIp(pair.second());
				assertNotNull(actualResult);
				assertTrue(!actualResult.equals(pair.first()));
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToLongInvalidLongFormat() {
		for (String address : listIncorrectIpv4Address) {
			long actualResult;
			try {
				actualResult = convertor.ipToLong(address);
				fail();
			} catch (InvalidIpDataFormat e) {
			}
		}
	}

	@Test
	public void testNullParam() throws InvalidIpDataFormat {
		try {
			long res = convertor.ipToLong(null);
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		}
	}

	public void crossCheckTest() {
		for (Pair<String, Long> pair : listCorrectSet) {
			try {
				long actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				String actualResultString = convertor.longToIp(actualResult);
				assertNotNull(actualResultString);
				assertEquals(pair.first(), actualResultString);

				String actualResult2 = convertor.longToIp(pair.second());
				assertNotNull(actualResult2);
				long actualResultLong = convertor.ipToLong(actualResult2);
				assertNotNull(actualResultLong);
				assertEquals((long) pair.second(), actualResultLong);

			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception but was InvalidIpDataFormat");
			}
		}
	}

	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest("testIpToLongCorrect"));
		suite.addTest(new Ipv4ConverterJavaTest("testLongToIpCorrect"));
		runner.doRun(suite);
	}
}