package com.example.proiect_android_tari.Util.test;

import static org.junit.Assert.*;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import junit.framework.TestCase;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import android.R;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;

import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv4ConverterTest extends TestCase {

	String ipAddress;
	long ipToLong;
	IpConvertorIpv4 convertor;

	@BeforeClass
	public void setUp() {
		ipAddress = "172.31.255.255";
		ipToLong = 2887778303L;
		convertor=new IpConvertorIpv4();
	}

	@Test
	public void testIpToLong() {
		long actualResult;
		try {
			actualResult = convertor.ipToLong(ipAddress);
			assertEquals(actualResult, ipToLong);
		} catch (InvalidIpDataFormat e) {
			fail();
		}
	}

	@Test
	public void testLongToIp() {
		String actualResult;
		try {
			actualResult = convertor.longToIp(ipToLong);
			assertEquals(actualResult, ipAddress);
		} catch (InvalidIpDataFormat e) {
			fail();
		}
	}
}
