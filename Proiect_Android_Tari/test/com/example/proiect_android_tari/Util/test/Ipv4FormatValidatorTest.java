package com.example.proiect_android_tari.Util.test;



import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import com.example.proiect_android_tari.Util.Ipv4FormatValidator;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv4FormatValidatorTest extends TestCase {

	public Ipv4FormatValidatorTest(String name) {
		super(name);
	}
	String[] correctIpAddress = { "122.34.12.12", "1.115.12.1", "1.23.5.23",
			"1.34.12.113" };
	String[] incorrectIpAddress = { "1222.34.123.123", "567.34.123.123",
			"1222.34.523.123" };

	private Ipv4FormatValidator validator;

	@Before
	public void setUp() {
		validator = new Ipv4FormatValidator();
	}

	@Test
	public void testCorrectForm() {
		for (int i = 0; i < correctIpAddress.length; i++) {
			assertEquals(true, validator.isValidIpFormat(correctIpAddress[i]));
		}
	}

	@Test
	public void testIncorrectForm() {
		for (int i = 0; i < incorrectIpAddress.length; i++) {
			assertEquals(false,
					validator.isValidIpFormat(incorrectIpAddress[i]));
		}
	}

	@Test(expected = InvalidParameterException.class)
	public void testNullParam() {
		try{
		validator.isValidIpFormat(null);
		fail("expected = InvalidParameterException.class");
		}catch(InvalidParameterException e){
		}
	}

}
