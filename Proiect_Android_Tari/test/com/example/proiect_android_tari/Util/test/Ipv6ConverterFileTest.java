package com.example.proiect_android_tari.Util.test;


import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import java_unit_test.Ipv4ConverterJavaTest;

import javax.xml.transform.stream.StreamResult;

import junit.framework.Assert;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import android.test.InstrumentationTestCase;

import com.example.proiect_android_tari.Util.IpConvertorIpv6;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv6ConverterFileTest extends InstrumentationTestCase {

	IpConvertorIpv6 convertor;
	Reader reader;
	BufferedReader br;

	public Ipv6ConverterFileTest() {
		super();
	}

	@Before
	public void setUp() throws IOException {
		convertor = new IpConvertorIpv6();
		reader = new InputStreamReader(getInstrumentation().getTargetContext().getAssets().open("Ipv6Set.txt"));
		br = new BufferedReader(reader);
	}
	
	@After
	public void tearDown() throws IOException{
		br.close();
		reader.close();
	}
	@Test
	public void testIpToLongCorrect() {
		String line;
		try {
			line = br.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			
			long []longs ={Long.parseLong(set[1]), Long.parseLong(set[2])};
			String expectedIp=set[0].toLowerCase();
			
			long []actualResult = convertor.ipToLong(expectedIp);
			
			assertNotNull(actualResult);
			assertEquals(2, actualResult.length);
			assertEquals(actualResult[0], longs[0]);
			assertEquals(actualResult[1], longs[1]);
			
			String actualResultString = convertor.longToIp(longs);
			assertNotNull(actualResult);
			assertEquals(expectedIp, actualResultString);
			
			//Cross-Check
			//1
			String actualResultString2 = convertor.longToIp(longs);
			assertNotNull(actualResultString2);
			long []actualResult2 = convertor.ipToLong(actualResultString);
			assertNotNull(actualResult2);
			assertEquals(2, actualResult.length);
			assertEquals(longs[0], actualResult[0]);
			assertEquals(longs[1], actualResult[1]);
			//2
			long []actualResult3 = convertor.ipToLong(expectedIp);
			assertNotNull(actualResult3);
			String actualResultString3 = convertor.longToIp(actualResult3);
			assertNotNull(actualResultString3);
			assertEquals(expectedIp,  actualResultString3);
			line = br.readLine();
		}
		} catch (IOException e) {
			fail("Resources not found");
		} catch (InvalidIpDataFormat ipe){
			fail("Invalida ip format exception");
		}
	}

	
//	public static void main(String[] args) {
//		TestRunner runner = new TestRunner();
//		TestSuite suite = new TestSuite();
//		suite.addTest(new Ipv6ConverterFileTest("testIpToLong"));
//		runner.doRun(suite);
//	}

}
