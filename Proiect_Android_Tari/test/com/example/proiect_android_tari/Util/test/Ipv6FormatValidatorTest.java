package com.example.proiect_android_tari.Util.test;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import com.example.proiect_android_tari.Util.Ipv4FormatValidator;
import com.example.proiect_android_tari.Util.Ipv6FormatValidator;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv6FormatValidatorTest extends TestCase {

	String[] correctIpAddress = { "3ffe:1900:4545:3:200:f8ff:fe21:67cf", "FE80:0:0:0:202:B3FF:FE1E:8329" };
	String[] incorrectIpAddress = { "3ffe:1900:4545:3:200:f8ff:fe21:67cf:1234", "3ffe:1900:4545:3:200:f8ff",
			":3:200:f8ff:fe21:67cf" };

	private Ipv6FormatValidator validator;

	@Before
	public void setUp() {
		validator = new Ipv6FormatValidator();
	}

	@Test
	public void testCorrectForm() {
		for (int i = 0; i < correctIpAddress.length; i++) {
			assertEquals(true, validator.isValidIpFormat(correctIpAddress[i]));
		}
	}

	@Test
	public void testIncorrectForm() {
		for (int i = 0; i < incorrectIpAddress.length; i++) {
			assertEquals(false,
					validator.isValidIpFormat(incorrectIpAddress[i]));
		}
	}

	@Test(expected = InvalidParameterException.class)
	public void testNullParam() {
		try{
		validator.isValidIpFormat(null);
		fail("expected = InvalidParameterException.class");
		}catch(InvalidParameterException e){
		}
	}

}
