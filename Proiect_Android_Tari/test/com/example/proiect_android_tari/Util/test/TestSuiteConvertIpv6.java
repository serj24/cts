package com.example.proiect_android_tari.Util.test;

import junit.framework.Test;
import junit.framework.TestSuite;

public class TestSuiteConvertIpv6 extends TestSuite {

	public static Test suite() {
		TestSuite suite = new TestSuite("Ipv6ConverterTests");
		suite.addTest(TestSuite.createTest(Ipv6ConverterFileTest.class,
				"testIpToLongCorrect"));
		suite.addTest(TestSuite.createTest(Ipv6ConverterFile2Test.class,
				"testIpToLongCorrect"));
		suite.addTest(TestSuite.createTest(Ipv6ConverterFile2Test.class,
				"testLongToIpCorrect"));
		suite.addTest(TestSuite.createTest(Ipv6ConverterFile2Test.class,
				"testIpToLongIncorrect"));
		suite.addTest(TestSuite.createTest(Ipv6ConverterFile2Test.class,
				"testLongToIpIncorrect"));
		suite.addTest(TestSuite.createTest(Ipv6ConverterFile2Test.class,
				"testIpToLongInvalidLongFormat"));
		suite.addTest(TestSuite.createTest(IpChainHandlerTest.class,
				"testIpToNumericLongIpv6HandlerCorrectAddresses"));
		suite.addTest(TestSuite.createTest(IpChainHandlerTest.class,
				"testChainHandlerIpv6First"));
		return suite;
	}

}
