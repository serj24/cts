package com.example.proiect_android_tari.Util.test;

import com.example.proiect_android_tari.Database.DAOTari;
import com.example.proiect_android_tari.Database.DAOTariTest;

import junit.framework.Test;
import junit.framework.TestSuite;

public class TestSuiteDaoTari extends TestSuite{

	public static Test suite() {
		TestSuite suite = new TestSuite("DaoTariConfiguration");
		suite.addTest(TestSuite.createTest(DAOTariTest.class,
				"testConstructorEmptyStringForDbName"));
		suite.addTest(TestSuite.createTest(DAOTariTest.class,
				"testConstructorNULLForDbName"));
		suite.addTest(TestSuite.createTest(DAOTariTest.class,
				"testConstructorNULLForContext"));
		suite.addTest(TestSuite.createTest(DAOTariTest.class,
				"testNullBdClosedDb"));
		suite.addTest(TestSuite.createTest(DAOTariTest.class,
				"testAlreadyClosedDb"));
		suite.addTest(TestSuite.createTest(DAOTariTest.class,
				"testOpenDb"));
		return suite;
	}
}
