package com.example.proiect_android_tari.Util.test;

import com.example.proiect_android_tari.Util.Ipv4FormatValidator;

import junit.framework.Test;
import junit.framework.TestSuite;


public class TestSuiteValidateIpv4 extends TestSuite{

	
	 public static Test suite () {
	        TestSuite suite = new TestSuite("Ipv4Tests");
	        suite.addTest(new IpValidatorTest("testCorrectIpv4Set"));
			suite.addTest(new IpValidatorTest("testStaticIncorrectIpv4"));
			suite.addTest(new IpValidatorTest("testInvalidFormatIpv4"));
			suite.addTest(new IpValidatorTest("testStaticCorrectIpv4"));
			suite.addTest(new Ipv4FormatValidatorTest("testCorrectForm"));
			suite.addTest(new Ipv4FormatValidatorTest("testIncorrectForm"));
			suite.addTest(TestSuite.createTest(Ipv4ConverterFileTest.class, "testLongToIpIncorrect"));
	        return suite;
	    }
	 
}
