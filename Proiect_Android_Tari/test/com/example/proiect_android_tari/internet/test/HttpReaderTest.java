package com.example.proiect_android_tari.internet.test;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.security.InvalidParameterException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.util.Log;

import com.example.proiect_android_tari.Retea.HttpReader;
import com.example.proiect_android_tari.Retea.PreluareTari;

public class HttpReaderTest extends TestCase {

	@Test
	public void testNullParam(){
		try{
		String response = HttpReader.getResponse(null);
		fail("expected InvalidParameterException");
		}catch(InvalidParameterException e){
		}
	}
	
	@Test
	public void testEmptyParam(){
		try{
		String response = HttpReader.getResponse("");
		fail("expected InvalidParameterException");
		}catch(InvalidParameterException e){
		}
	}
	
	@Test
	public void testGetResponse() {	
		String url ="http://oorsprong.org/websamples.countryinfo/CountryInfoService.wso/FullCountryInfoAllCountries";
		String response = HttpReader.getResponse(url);
		assertTrue(isValidXml(response));
		Log.v("test", response);
	}

	public boolean isValidXml(String xmlString) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		Document doc;
		try {
			builder = factory.newDocumentBuilder();
			doc = builder.parse(new InputSource(new StringReader(xmlString)));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
