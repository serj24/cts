package java_unit_test;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;
import java.security.acl.LastOwnerException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.example.proiect_android_tari.Util.IpConverterAdaptor;
import com.example.proiect_android_tari.Util.IpConvertor;
import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class IpConverterAdaptorTest extends TestCase{

	
	private String IpAddress;
	private long LongAddressValue;
	@Before
	public void setUp(){
		System.setProperty("dexmaker.dexcache", "");
		IpAddress = "122.34.123.32";
		LongAddressValue = 2049080096;		
	}
	@Test(expected=InvalidParameterException.class)
	public void testConstructor() {
		try{
		IpConvertor adaptor=new IpConverterAdaptor(null);
		fail("expected=InvalidParameterException.class");
		}catch(InvalidParameterException e){
			
		}
	}
	
	@Test
	public void testIpToLong() throws InvalidIpDataFormat {
		IpConvertorIpv4 convertor = mock(IpConvertorIpv4.class);
		when(convertor.ipToLong(IpAddress)).thenReturn(LongAddressValue);
		
		IpConvertor adaptor=new IpConverterAdaptor(convertor);
		long[]expected={LongAddressValue};
		assertArrayEquals(expected, adaptor.ipToLong(IpAddress));
	}
	
	@Test
	public void testlongToIp() throws InvalidIpDataFormat {
		IpConvertorIpv4 convertor = mock(IpConvertorIpv4.class);
		when(convertor.longToIp(LongAddressValue)).thenReturn(IpAddress);
		
		IpConvertor adaptor=new IpConverterAdaptor(convertor);
		long[]longs={LongAddressValue};
		assertEquals(IpAddress, adaptor.longToIp(longs));
	}
	
	@Test(expected=InvalidParameterException.class)
	public void testIpToLongNullParam() throws InvalidIpDataFormat {
		IpConvertorIpv4 convertor = mock(IpConvertorIpv4.class);
		when(convertor.ipToLong(IpAddress)).thenThrow(InvalidParameterException.class);
		IpConvertor adaptor=new IpConverterAdaptor(convertor);
		try{
		long []res=adaptor.ipToLong(IpAddress);
		fail("expected=InvalidParameterException");
		}catch(InvalidParameterException e){
			
		}
	}
	
	@Test(expected=InvalidParameterException.class)
	public void testlongToIpNullParam() throws InvalidIpDataFormat {
		IpConvertorIpv4 convertor = mock(IpConvertorIpv4.class);
		IpConvertor adaptor=new IpConverterAdaptor(convertor);
		try{
		String ip = adaptor.longToIp(null);
		fail("expected=InvalidParameterException");
		}catch(InvalidParameterException e){
			
		}
	}
	
}