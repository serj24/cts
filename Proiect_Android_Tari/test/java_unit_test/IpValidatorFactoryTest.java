package java_unit_test;

import java.text.Normalizer.Form;

import org.junit.Test;

import com.example.proiect_android_tari.Util.IIpFormatValidator;
import com.example.proiect_android_tari.Util.IpFormat;
import com.example.proiect_android_tari.Util.IpValidatorFactory;
import com.example.proiect_android_tari.Util.Ipv4FormatValidator;
import com.example.proiect_android_tari.Util.Ipv6FormatValidator;

import junit.framework.TestCase;

public class IpValidatorFactoryTest extends TestCase {

	@Test
	public void testIpv4Validator() {
		IIpFormatValidator validator = IpValidatorFactory
				.createValidator(IpFormat.Ipv4);
		assertTrue(Ipv4FormatValidator.class.isInstance(validator));
	}

	@Test
	public void testNegativeIpv4Validator() {
		IIpFormatValidator validator = IpValidatorFactory
				.createValidator(IpFormat.Ipv6);
		assertEquals(false, Ipv4FormatValidator.class.isInstance(validator));
	}
	@Test
	public void testIpv6Validator() {
		IIpFormatValidator validator = IpValidatorFactory
				.createValidator(IpFormat.Ipv6);
		assertTrue(Ipv6FormatValidator.class.isInstance(validator));
	}
	@Test
	public void testNegativeIpv6Validator() {
		IIpFormatValidator validator = IpValidatorFactory
				.createValidator(IpFormat.Ipv4);
		assertEquals(false, Ipv6FormatValidator.class.isInstance(validator));
	}
	
}
