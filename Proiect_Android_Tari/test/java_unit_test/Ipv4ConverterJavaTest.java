package java_unit_test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.Pair;
import com.example.proiect_android_tari.Util.test.Ipv4ConverterTest;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv4ConverterJavaTest extends TestCase {

	static List<Pair<String, Long>> listCorrectSet;
	static List<Pair<String, Long>> listIncorrectLongValues;
	static List<String> listIncorrectIpv4Address;
	IpConvertorIpv4 convertor;

	public Ipv4ConverterJavaTest(String name) {
		super(name);
	}

	private ArrayList<Pair<String, Long>> getCorrectSet() throws IOException {
		ArrayList<Pair<String, Long>> list = new ArrayList<Pair<String, Long>>();
		BufferedReader br = new BufferedReader(new FileReader("Ipv4Set.txt"));
		String line = br.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			list.add(new Pair(set[0], Long.parseLong(set[1])));
			line = br.readLine();
		}
		return list;
	}

	private ArrayList<Pair<String, Long>> getIncorrectLongValues()
			throws IOException {
		ArrayList<Pair<String, Long>> list = new ArrayList<Pair<String, Long>>();
		BufferedReader br = new BufferedReader(new FileReader(
				"IpIv4IncorectLongValues.txt"));
		String line = br.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			list.add(new Pair(set[0], Long.parseLong(set[1])));
			line = br.readLine();
		}
		return list;
	}

	private ArrayList<String> getIncorrectIpAddress()
			throws IOException {
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(
				"Ipv4IncorectAddresses.txt"));
		String line = br.readLine();
		while (line != null) {
			list.add(line);
			line = br.readLine();
		}
		return list;
	}

	@Before
	public void setUp() {
		convertor = new IpConvertorIpv4();
		try {
			listCorrectSet = getCorrectSet();
			listIncorrectLongValues = getIncorrectLongValues();
			listIncorrectIpv4Address = getIncorrectIpAddress();
		} catch (FileNotFoundException e) {
			fail("Resourse file not found!");
		} catch (IOException e) {
			fail("Resourse file not found!");
		}
	}

	@Test
	public void testIpToLongCorrect() {
		for (Pair<String, Long> pair : listCorrectSet) {
			long actualResult;
			try {
				actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				assertEquals(actualResult, (long) pair.second());
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testLongToIpCorrect() {
		for (Pair<String, Long> pair : listCorrectSet) {
			String actualResult;
			try {
				actualResult = convertor.longToIp(pair.second());
				assertNotNull(actualResult);
				assertEquals(actualResult, pair.first());
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testIpToLongIncorrect() {
		for (Pair<String, Long> pair : listIncorrectLongValues) {
			long actualResult;
			try {
				actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				assertTrue(actualResult != (long) pair.second());
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testLongToIpIncorrect() {
		for (Pair<String, Long> pair : listIncorrectLongValues) {
			String actualResult;
			try {
				actualResult = convertor.longToIp(pair.second());
				assertNotNull(actualResult);
				assertTrue(!actualResult.equals(pair.first()));
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToLongInvalidLongFormat() {
		for (String address : listIncorrectIpv4Address) {
			long actualResult;
			try {
				actualResult = convertor.ipToLong(address);
				fail();
			} catch (InvalidIpDataFormat e) {
			}
		}
	}
   @Test
   public void testNullParam() throws InvalidIpDataFormat{
	   try {
		   long res = convertor.ipToLong(null);
			fail("expected=InvalidParameterException.class");
		} catch (InvalidParameterException e) {
		} 
   }
   public void crossCheckTest(){
	   for (Pair<String, Long> pair : listCorrectSet) {
			try {
				long actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				String actualResultString=convertor.longToIp(actualResult);
				assertNotNull(actualResultString);
				assertEquals(pair.first(), actualResultString);
				
				String actualResult2 = convertor.longToIp(pair.second());
				assertNotNull(actualResult2);
				long actualResultLong = convertor.ipToLong(actualResult2);
				assertNotNull(actualResultLong);
				assertEquals((long)pair.second(), actualResultLong);
				
			} catch (InvalidIpDataFormat e) {
				fail("expected = no exception but was InvalidIpDataFormat");
			}
		}
   }
	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest("testIpToLongCorrect"));
		suite.addTest(new Ipv4ConverterJavaTest("testLongToIpCorrect"));
		runner.doRun(suite);
	}
}