package java_unit_test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.example.proiect_android_tari.Util.IpConvertorIpv6;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv6ConverterJavaFileTest extends TestCase {

	IpConvertorIpv6 convertor;
	FileReader reader;
	BufferedReader br;

	public Ipv6ConverterJavaFileTest(String name) {
		super(name);
	}

	@Before
	public void setUp() throws FileNotFoundException {
		convertor = new IpConvertorIpv6();
		reader = new FileReader("Ipv6Set.txt");
		br = new BufferedReader(reader);
	}

	@Test
	public void testIpToLongCorrect() {
		String line;
		try {
			line = br.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			
			long []longs ={Long.parseLong(set[1]), Long.parseLong(set[2])};
			String expectedIp=set[0].toLowerCase();
			
			long []actualResult = convertor.ipToLong(expectedIp);
			
			assertNotNull(actualResult);
			assertArrayEquals(actualResult, longs);
			
			String actualResultString = convertor.longToIp(longs);
			assertNotNull(actualResult);
			assertEquals(expectedIp, actualResultString);
			
			//Cross-Check
			//1
			String actualResultString2 = convertor.longToIp(longs);
			assertNotNull(actualResultString2);
			long []actualResult2 = convertor.ipToLong(actualResultString);
			assertNotNull(actualResult2);
			assertArrayEquals(longs, actualResult2);
			//2
			long []actualResult3 = convertor.ipToLong(expectedIp);
			assertNotNull(actualResult3);
			String actualResultString3 = convertor.longToIp(actualResult3);
			assertNotNull(actualResultString3);
			assertEquals(expectedIp,  actualResultString3);
			line = br.readLine();
		}
		} catch (IOException e) {
			fail("Resources not found");
		} catch (InvalidIpDataFormat ipe){
			fail("Invalida ip format exception");
		}
	}

	@After
	public void tearDown() throws IOException{
		br.close();
		reader.close();
	}
	
	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest("testIpToLong"));
		suite.addTest(new Ipv4ConverterJavaTest("testLongToIp"));
		runner.doRun(suite);
	}

}
