package java_unit_test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

import org.junit.Before;
import org.junit.Test;

import com.example.proiect_android_tari.Util.IpConvertorIpv4;
import com.example.proiect_android_tari.Util.IpConvertorIpv6;
import com.example.proiect_android_tari.Util.Pair;
import com.example.proiect_android_tari.Util.Pair2;
import com.example.proiect_android_tari.exceptions.InvalidIpDataFormat;

public class Ipv6ConverterJavaTest  extends TestCase {

	static List<Pair2<String, Long, Long>> listCorrectSet;
	static List<Pair2<String, Long, Long>> listIncorrectLongValues;
	static List<String> listIncorrectIpv4Address;
	IpConvertorIpv6 convertor;

	public Ipv6ConverterJavaTest(String name) {
		super(name);
	}

	private ArrayList<Pair2<String, Long, Long>> getCorrectSet() throws IOException {
		ArrayList<Pair2<String, Long, Long>> list = new ArrayList<Pair2<String, Long, Long>>();
		BufferedReader br = new BufferedReader(new FileReader("Ipv6Set.txt"));
		String line = br.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			list.add(new Pair2(set[0], Long.parseLong(set[1]), Long.parseLong(set[2])));
			line = br.readLine();
		}
		return list;
	}

	private ArrayList<Pair2<String, Long, Long>> getIncorrectLongValues()
			throws IOException {
		ArrayList<Pair2<String, Long, Long>> list = new ArrayList<Pair2<String, Long, Long>>();
		BufferedReader br = new BufferedReader(new FileReader(
				"IpIv6IncorectLongValues.txt"));
		String line = br.readLine();
		while (line != null) {
			String[] set = line.split(" ");
			list.add(new Pair2(set[0], Long.parseLong(set[1]), Long.parseLong(set[2])));
			line = br.readLine();
		}
		return list;
	}

	private ArrayList<String> getIncorrectIpAddress()
			throws IOException {
		ArrayList<String> list = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new FileReader(
				"Ipv6IncorectAddresses.txt"));
		String line = br.readLine();
		while (line != null) {
			list.add(line);
			line = br.readLine();
		}
		return list;
	}

	@Before
	public void setUp() {
		convertor = new IpConvertorIpv6();
		try {
			listCorrectSet = getCorrectSet();
			listIncorrectLongValues = getIncorrectLongValues();
			listIncorrectIpv4Address = getIncorrectIpAddress();
		} catch (FileNotFoundException e) {
			fail("Resourse file not found!");
		} catch (IOException e) {
			fail("Resourse file not found!");
		}
	}

	@Test
	public void testIpToLongCorrect() {
		for (Pair2<String, Long, Long> pair : listCorrectSet) {
			long[] actualResult;
			try {
				long []longs ={pair.second(), pair.third()};
				actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				assertArrayEquals(actualResult, longs);
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testLongToIpCorrect() {
		for (Pair2<String, Long, Long> pair : listCorrectSet) {
			String actualResult;
			try {
				long []longs ={pair.second(), pair.third()};
				actualResult = convertor.longToIp(longs);
				assertNotNull(actualResult);
				assertEquals(pair.first().toLowerCase(), actualResult);
				System.out.println(actualResult);
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testIpToLongIncorrect() {
		for (Pair2<String, Long, Long> pair : listIncorrectLongValues) {
			long[] actualResult;
			try {
				long []longs={pair.second(), pair.third()};
				actualResult = convertor.ipToLong(pair.first());
				assertNotNull(actualResult);
				assertTrue(actualResult.length == 2);
				assertTrue(longs[0] != actualResult[0]);
				assertTrue(longs[1] != actualResult[1]);
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test
	public void testLongToIpIncorrect() {
		for (Pair2<String, Long, Long> pair : listIncorrectLongValues) {
			String actualResult;
			try {
				long []longs={pair.second(), pair.third()};
				actualResult = convertor.longToIp(longs);
				assertNotNull(actualResult);
				assertTrue(!actualResult.equals(pair.first()));
			} catch (InvalidIpDataFormat e) {
				fail();
			}
		}
	}

	@Test(expected = InvalidIpDataFormat.class)
	public void testIpToLongInvalidLongFormat() {
		for (String address : listIncorrectIpv4Address) {
			long[] actualResult;
			try {
				actualResult = convertor.ipToLong(address);
				fail();
			} catch (InvalidIpDataFormat e) {
			}
		}
	}
   @Test
   public void testNullIpParam() throws InvalidIpDataFormat{
	   try {
		   long[] res = convertor.ipToLong(null);
			fail();
		} catch (InvalidParameterException e) {
		} 
   }
   @Test
   public void testNullLongParam() throws InvalidIpDataFormat{
	   try {
		   String res=convertor.longToIp(null);
			fail("expected = InvalidParameterException");
		} catch (InvalidParameterException e) {
		} 
   }

	public static void main(String[] args) {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new Ipv4ConverterJavaTest("testIpToLong"));
		suite.addTest(new Ipv4ConverterJavaTest("testLongToIp"));
		runner.doRun(suite);
	}

}
